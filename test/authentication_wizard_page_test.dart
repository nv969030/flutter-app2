// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/authentication-wizard/authentication_wizard_page.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';
import 'package:blauwe_knop/repositories/universal_link/repository.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

class MockSchuldenRequestSecureStorage extends Mock
    implements SchuldenRequestSecureStorage {}

class MockCallbackRepository extends Mock implements CallBackRepository {}

class MockSchuldenClient extends Mock implements AppDebtProcessClient {}

class MockKeyPairRepository extends Mock implements KeyPairRepository {}

class MockCallBackFunction extends Mock {
  void call();
}

void main() {
  testWidgets('manually selecting an organization',
      (WidgetTester tester) async {
    var schuldenRequestSecureStorage = MockSchuldenRequestSecureStorage();

    when(schuldenRequestSecureStorage.getRequest())
        .thenAnswer((_) async => null);

    when(
      schuldenRequestSecureStorage.saveRequest(
        SchuldenRequest(
            id: 'mock-request-id',
            registratorOrganization: RegistratorOrganization(
              oin: '00000000000000000002',
              name: 'mock-aggregator',
              debtRequestProcessUrl: 'https://api.com',
            ),
            manualOrganizationSelection: false,
            sourceOrganizations: {
              '00000000000000000001': SourceOrganization(
                oin: '00000000000000000001',
                name: 'mock-org',
                apiBaseUrl: '',
              )
            }),
      ),
    ).thenAnswer((_) => null);

    var callBackSteam = StreamController<String>();

    var callbackRepository = MockCallbackRepository();
    when(callbackRepository.getUniversalLinkStream())
        .thenAnswer((_) => callBackSteam.stream);

    var schuldenClient = MockSchuldenClient();

    when(
      schuldenClient.getRequestIdForExchangeToken(
          'https://api.com', 'mock-exchange-token'),
    ).thenAnswer(
      (_) => Future<String>.value('mock-request-id'),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetPublicKeyPEM()).thenAnswer(
      (_) => Future.value('mock-public-key'),
    );

    var appDebtRepository = AppDebtProcessRepository(client: schuldenClient);
    var mockCallBack = MockCallBackFunction();
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [
                  Organization(
                    name: 'mock-org',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '00000000000000000001',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                  Organization(
                    name: 'mock-aggregator',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '00000000000000000002',
                    isRegistrator: true,
                    registratorUrl: 'https://api.com',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                ],
              ),
            ),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestSecureStorage,
          ),
          Provider<CallBackRepository>(
            create: (_) => callbackRepository,
          ),
          Provider<AppDebtProcessRepository>(
            create: (_) => appDebtRepository,
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          )
        ],
        child: MaterialApp(
          theme: Themes.blauweKnop(),
          home: AuthenticationWizardPage(
            onWizardCompletedHandler: () => mockCallBack.call(),
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();

    // Stap 1 / 4
    expect(
        find.text('Nee, ik wil zelf de organisaties kiezen'), findsOneWidget);
    await tester.tap(find.text('Nee, ik wil zelf de organisaties kiezen'));

    await tester.pumpAndSettle();

    // Stap 2 / 4
    expect(find.text('mock-org'), findsOneWidget);
    expect(find.text('Volgende'), findsOneWidget);

    await tester.tap(find.text('mock-org'));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Volgende'));
    await tester.pumpAndSettle();

    // Stap 3 / 4
    expect(find.text('mock-aggregator'), findsOneWidget);
    expect(
        find.text(
            'Wie mag uw verzoek om schuldinformatie doorgeven aan deze organisaties?'),
        findsOneWidget);
    expect(find.byKey(Key('filter-aggregator-field')), findsOneWidget);

    await tester.enterText(find.byKey(Key('filter-aggregator-field')),
        'non-existing-aggregator-name');
    await tester.pumpAndSettle();

    expect(find.text('mock-aggregator'), findsNothing);

    await tester.enterText(find.byKey(Key('filter-aggregator-field')), '');
    await tester.pumpAndSettle();

    await tester.tap(find.text('mock-aggregator'));
    await tester.pumpAndSettle();

    // Stap 4 / 4
    expect(find.text('Verzoek indienen'), findsOneWidget);
    await tester.tap(find.text('Verzoek indienen'));

    callBackSteam.add(
        'blauweknop.app.login://login?requestExchangeToken=mock-exchange-token');
    await tester.pumpAndSettle();
    verify(mockCallBack.call()).called(1);
  });
}
