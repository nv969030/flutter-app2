// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/app.dart';
import 'package:blauwe_knop/models/faq.dart';
import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/schulden_overzicht_page.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/select_flow.dart';
import 'package:blauwe_knop/pages/splash_screen/splash_screen_page.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/repositories/session_process/responses/complete_challenge_response.dart';
import 'package:blauwe_knop/repositories/session_process/responses/start_challenge_response.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

class MockSchuldenClient extends Mock implements AppDebtProcessClient {}

class MockSchuldenRequestRepository extends Mock
    implements SchuldenRequestRepository {}

class MockAppLinkRepository extends Mock implements AppLinkRepository {}

class MockDebtRepository extends Mock implements DebtRepository {}

class MockKeyPairRepository extends Mock implements KeyPairRepository {}

class MockSessionProcessRepository extends Mock
    implements SessionProcessRepository {}

class MockCallBackFunction extends Mock {
  void call();
}

void main() {
  testWidgets('start up app', (WidgetTester tester) async {
    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    var schemeRepository = SchemeRepository(
      client: SchemeMemoryClient(
        organizations: [
          Organization(
            name: 'mock-org',
            apiUrl: '',
            loginUrl: '',
            oin: '00000000000000000000',
            isRegistrator: false,
            registratorUrl: '',
            appLinkProcessUrl: '',
            sessionProcessUrl: '',
          ),
        ],
      ),
    );

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    var mockSessionProcessRepository = MockSessionProcessRepository();
    when(mockSessionProcessRepository.startChallenge('', null)).thenAnswer(
      (_) => Future<StartChallengeResponse>.value(null),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: MockSchuldenClient(),
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<SchemeRepository>(
            create: (_) => schemeRepository,
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => mockSessionProcessRepository,
          )
        ],
        child: BlauweKnopApp(),
      ),
    );

    expect(find.byType(SplashScreenPage), findsOneWidget);

    await tester.pumpAndSettle();
    expect(find.byType(SchuldenOverzichtPage), findsOneWidget);
  });

  testWidgets('start up app without previous debt request',
      (WidgetTester tester) async {
    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    var schemeRepository = SchemeRepository(
      client: SchemeMemoryClient(
        organizations: [
          Organization(
            name: 'mock-org',
            apiUrl: '',
            loginUrl: '',
            oin: '00000000000000000000',
            isRegistrator: false,
            registratorUrl: '',
            appLinkProcessUrl: '',
            sessionProcessUrl: '',
          ),
        ],
      ),
    );
    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: MockSchuldenClient(),
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<SchemeRepository>(
            create: (_) => schemeRepository,
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => MockSessionProcessRepository(),
          )
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();
    expect(find.text('Stap 1: Kies organisaties'), findsOneWidget);
  });

  testWidgets('start up with previous schulden unfinished request found',
      (WidgetTester tester) async {
    var schuldenClient = MockSchuldenClient();
    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(
        SchuldenRequest(
          id: null,
          registratorOrganization: RegistratorOrganization(
            name: 'mock-org',
            oin: '001',
            debtRequestProcessUrl: '',
          ),
          sourceOrganizations: {},
        ),
      ),
    );

    var appDebtRepository = AppDebtProcessRepository(
      client: schuldenClient,
    );

    var schemeRepository = SchemeRepository(
      client: SchemeMemoryClient(
        organizations: [
          Organization(
            name: 'mock-org',
            apiUrl: '',
            loginUrl: '',
            oin: '001',
            isRegistrator: false,
            registratorUrl: '',
            appLinkProcessUrl: '',
            sessionProcessUrl: '',
          ),
        ],
      ),
    );

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    var authorizationRepository = AuthorizationRepository();

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(create: (_) => appDebtRepository),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<AuthorizationRepository>(
            create: (_) => authorizationRepository,
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<SchemeRepository>(
            create: (_) => schemeRepository,
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => MockSessionProcessRepository(),
          ),
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();

    expect(find.text('Uw verzoek is nog niet ingediend'), findsOneWidget);
    expect(find.byKey(SettingsPageLinkKey), findsOneWidget);

    await tester.pump(Duration(seconds: 3));
  });

  testWidgets('start app with previous debt request',
      (WidgetTester tester) async {
    var schuldenClient = MockSchuldenClient();

    var schuldenRequestRepository = MockSchuldenRequestRepository();

    var appLinkRepository = MockAppLinkRepository();

    var debtRequestProcess = MockDebtRepository();

    var saveRequestCalled = MockCallBackFunction();

    var saveDebtCalled = MockCallBackFunction();
    var mockRequestID = '1';
    var mockRegistratorOIN = '02';

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(
        SchuldenRequest(
          id: mockRequestID,
          manualOrganizationSelection: true,
          registratorOrganization: RegistratorOrganization(
            name: 'mock-registrator-org',
            oin: mockRegistratorOIN,
          ),
          sourceOrganizations: {
            '01': SourceOrganization(
              oin: '01',
              name: 'mock-org-wizard',
            )
          },
        ),
      ),
    );

    var mockLinkProcessURL = 'http://mock.app.link.process.url';
    var mockOIN = '01';
    var mockLinkToken = 'linkToken';

    when(appLinkRepository.linkRequest(
      mockLinkProcessURL,
      mockRequestID,
      mockRegistratorOIN,
      '',
    )).thenAnswer((_) => Future<String>.value(mockLinkToken));

    var mockSchuld = SchuldenOverzicht(
      organisatie: 'mock-org-wizard',
      saldoDatumLaatsteSchuld: '',
      schulden: [
        SchuldDetail(
          type: 'schuld',
          saldo: 1000,
          vorderingOvergedragen: false,
        )
      ],
      totaalSaldo: 1000,
    );

    when(schuldenClient.getSchuldenOverzicht(
      'http://mock.api.url',
      mockLinkToken,
    )).thenAnswer(
      (_) => Future.value(mockSchuld),
    );

    when(debtRequestProcess.saveDebt(
      mockOIN,
      mockSchuld,
    )).thenAnswer((_) async {
      saveDebtCalled.call();
    });

    when(schuldenRequestRepository.saveRequest(
      any,
    )).thenAnswer((_) async {
      saveRequestCalled.call();
    });

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetPublicKeyPEM()).thenAnswer(
      (_) => Future.value('mock-publickey'),
    );
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    var uniLinkStreamController = StreamController<String>();

    var mockSessionProcessRepository = MockSessionProcessRepository();
    when(mockSessionProcessRepository.startChallenge('', 'mock-publickey'))
        .thenAnswer(
      (_) => Future<StartChallengeResponse>.value(
        StartChallengeResponse(random: 'random', date: 1),
      ),
    );
    when(mockSessionProcessRepository.completeChallenge(any, any, any))
        .thenAnswer(
      (_) => Future<CompleteChallengeResponse>.value(
        CompleteChallengeResponse(
          sessionToken: '',
        ),
      ),
    );

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: schuldenClient,
              uniLinkStream: uniLinkStreamController.stream,
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => debtRequestProcess,
          ),
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [
                  Organization(
                    name: 'mock-org-wizard',
                    apiUrl: 'http://mock.api.url',
                    loginUrl: '',
                    oin: '01',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: 'http://mock.app.link.process.url',
                    sessionProcessUrl: '',
                  ),
                  Organization(
                    name: 'mock-org-manual-login',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '02',
                    isRegistrator: true,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                ],
              ),
            ),
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => appLinkRepository,
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => mockSessionProcessRepository,
          ),
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();
    expect(find.text('mock-org-wizard'), findsOneWidget);
    expect(find.text('€ 10,00'), findsNWidgets(2));
    expect(find.text('mock-org-manual-login'), findsOneWidget);
    verify(saveDebtCalled.call()).called(1);
    verify(saveRequestCalled.call()).called(1);
    await tester.pump(Duration(seconds: 3));
  });

  testWidgets('start app and do manual login', (WidgetTester tester) async {
    var schuldenClient = MockSchuldenClient();

    when(schuldenClient.getSchuldenOverzicht('', 'demo-session-token'))
        .thenAnswer(
      (_) async => SchuldenOverzicht(
        organisatie: 'mock-org',
        saldoDatumLaatsteSchuld: '2019-06-05T00:00:00Z',
        totaalSaldo: 0,
        schulden: <SchuldDetail>[],
      ),
    );

    when(schuldenClient.createAuthToken(''))
        .thenAnswer((_) async => Future<String>.value('demo-auth-token'));

    when(schuldenClient.exchangeAuthToken('', 'demo-auth-token')).thenAnswer(
      (_) async => Future<String>.value('demo-session-token'),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    var uniLinkStreamController = StreamController<String>();

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: schuldenClient,
              uniLinkStream: uniLinkStreamController.stream,
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [
                  Organization(
                    name: 'mock-org-wizard',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '01',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                  Organization(
                    name: 'mock-org-manual-login',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '02',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                ],
              ),
            ),
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => MockSessionProcessRepository(),
          )
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();

    await tester.tap(find.text(' per organisatie inloggen'));
    await tester.pumpAndSettle();

    expect(find.text('mock-org-wizard'), findsOneWidget);
    expect(find.text('mock-org-manual-login'), findsOneWidget);

    await tester.tap(find.text('mock-org-manual-login'));
    await tester.pumpAndSettle();

    expect(find.text('Inloggen'), findsOneWidget);
    await tester.tap(find.text('Inloggen'));

    await tester.pumpAndSettle();
    uniLinkStreamController
        .add('blauweknop.app.login://login?organization=mock-org-manual-login');

    await tester.pumpAndSettle();

    expect(find.text('Schuldenoverzicht'), findsOneWidget);
  }, skip: true);

  testWidgets('start app and do failing manual login',
      (WidgetTester tester) async {
    var schuldenClient = MockSchuldenClient();

    when(schuldenClient.createAuthToken('https://my-api.com'))
        .thenThrow('failed to create auth token');

    var uniLinkStreamController = StreamController<String>();

    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: schuldenClient,
              uniLinkStream: uniLinkStreamController.stream,
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [
                  Organization(
                    name: 'mock-org-wizard',
                    apiUrl: '',
                    loginUrl: '',
                    oin: '01',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                  Organization(
                    name: 'mock-org-manual-login',
                    apiUrl: 'https://my-api.com',
                    loginUrl: '',
                    oin: '02',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                ],
              ),
            ),
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => MockSessionProcessRepository(),
          )
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();

    await tester.tap(find.text(' per organisatie inloggen'));
    await tester.pumpAndSettle();

    expect(find.text('mock-org-wizard'), findsOneWidget);
    expect(find.text('mock-org-manual-login'), findsOneWidget);

    await tester.tap(find.text('mock-org-manual-login'));
    await tester.pumpAndSettle();

    expect(find.text('Inloggen'), findsOneWidget);
    await tester.tap(find.text('Inloggen'));

    await tester.pumpAndSettle();

    expect(find.text('Oeps, er gaat iets fout'), findsOneWidget);
  }, skip: true);

  testWidgets('login flow error - failed to create auth token',
      (WidgetTester tester) async {
    var client = MockSchuldenClient();

    when(client.createAuthToken('https://my-api.com'))
        .thenThrow('failed to create auth token');

    var uniLinkStreamController = StreamController<String>();

    var schuldenRequestRepository = MockSchuldenRequestRepository();

    when(schuldenRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.GetHasKeyPairStream()).thenAnswer(
      (_) => Stream.value(true),
    );

    var faqRepository = FaqRepository(
      client: FaqMemoryClient(
        faqs: [Faq(question: 'Question #1', answer: 'Answer #1')],
      ),
    );

    await tester.pumpWidget(
      MultiProvider(
        providers: [
          Provider<AppDebtProcessRepository>(
            create: (_) => AppDebtProcessRepository(
              client: client,
              uniLinkStream: uniLinkStreamController.stream,
            ),
          ),
          Provider<DebtRepository>(
            create: (_) => MockDebtRepository(),
          ),
          Provider<SchemeRepository>(
            create: (_) => SchemeRepository(
              client: SchemeMemoryClient(
                organizations: [
                  Organization(
                    name: 'mock-org',
                    loginUrl: '',
                    apiUrl: 'https://my-api.com',
                    oin: '00000000000000000000',
                    isRegistrator: false,
                    registratorUrl: '',
                    appLinkProcessUrl: '',
                    sessionProcessUrl: '',
                  ),
                ],
              ),
            ),
          ),
          Provider<FaqRepository>(
            create: (_) => faqRepository,
          ),
          Provider<AuthorizationRepository>(
            create: (_) => AuthorizationRepository(),
          ),
          Provider<SchuldenRequestRepository>(
            create: (_) => schuldenRequestRepository,
          ),
          Provider<AppLinkRepository>(
            create: (_) => MockAppLinkRepository(),
          ),
          Provider<KeyPairRepository>(
            create: (_) => mockKeyPairRepository,
          ),
          Provider<SessionProcessRepository>(
            create: (_) => MockSessionProcessRepository(),
          )
        ],
        child: BlauweKnopApp(),
      ),
    );

    await tester.pumpAndSettle();

    await tester.tap(find.text(' per organisatie inloggen'));
    await tester.pumpAndSettle();

    await tester.tap(find.text('mock-org'));
    await tester.pumpAndSettle();

    await tester.tap(find.text('Inloggen'));
    await tester.pumpAndSettle();

    expect(find.text('Oeps, er gaat iets fout'), findsOneWidget);
  }, skip: true);
}
