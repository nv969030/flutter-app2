// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/pages/settings/settings_page.dart';
import 'package:blauwe_knop/pages/settings/widgets/info_remove_debt_request.dart';
import 'package:blauwe_knop/pages/settings/widgets/info_remove_personal_data.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

class MockSchuldenRequestRepository extends Mock
    implements SchuldenRequestRepository {}

class MockDebtRepository extends Mock implements DebtRepository {}

class MockKeyPairRepository extends Mock implements KeyPairRepository {}

void main() {
  testWidgets('without previous debt request', (WidgetTester tester) async {
    var debtRequestRepository = MockSchuldenRequestRepository();

    when(debtRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    await tester.pumpWidget(
      MaterialApp(
        theme: Themes.blauweKnop(),
        home: MultiProvider(
          providers: [
            Provider<SchuldenRequestRepository>(
              create: (context) => debtRequestRepository,
            ),
            Provider<DebtRepository>(
              create: (context) => MockDebtRepository(),
            ),
            Provider<KeyPairRepository>(
              create: (context) => MockKeyPairRepository(),
            )
          ],
          child: SettingsPage(
            onRequestRemoved: () {},
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();
    expect(find.text('Geen openstaand verzoek.'), findsOneWidget);
  });

  testWidgets('with previous debt request', (WidgetTester tester) async {
    var mockDebtRequestRepository = MockSchuldenRequestRepository();

    when(mockDebtRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(SchuldenRequest(
        registratorOrganization:
            RegistratorOrganization(name: 'dummy-registrator-name'),
        sourceOrganizations: {
          '': SourceOrganization(),
        },
      )),
    );
    when(mockDebtRequestRepository.deleteRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(),
    );
    await tester.pumpWidget(
      MaterialApp(
        theme: Themes.blauweKnop(),
        home: MultiProvider(
          providers: [
            Provider<SchuldenRequestRepository>(
              create: (context) => mockDebtRequestRepository,
            ),
            Provider<DebtRepository>(
              create: (context) => MockDebtRepository(),
            ),
            Provider<KeyPairRepository>(
              create: (context) => MockKeyPairRepository(),
            )
          ],
          child: SettingsPage(
            onRequestRemoved: () {},
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();
    expect(
        find.text(
            'Gemeente dummy-registrator-name stuurt dit verzoek naar 1 organisatie.'),
        findsOneWidget);

    await tester.tap(find.byKey(RemoveDebtRequestButtonKey));
    await tester.pumpAndSettle();

    verify(mockDebtRequestRepository.deleteRequest()).called(1);
  });

  testWidgets('removing all personal data', (WidgetTester tester) async {
    var mockDebtRequestRepository = MockSchuldenRequestRepository();

    when(mockDebtRequestRepository.getRequest()).thenAnswer(
      (_) => Future<SchuldenRequest>.value(null),
    );

    when(mockDebtRequestRepository.deleteRequest()).thenAnswer(
      (_) => Future.value(),
    );

    var mockDebtRepository = MockDebtRepository();
    when(mockDebtRepository.deleteAllDebt())
        .thenAnswer((realInvocation) => Future.value());

    var mockKeyPairRepository = MockKeyPairRepository();
    when(mockKeyPairRepository.DeleteKeyPair()).thenAnswer(
      (_) => Future.value(),
    );

    await tester.pumpWidget(
      MaterialApp(
        theme: Themes.blauweKnop(),
        home: MultiProvider(
          providers: [
            Provider<SchuldenRequestRepository>(
              create: (context) => mockDebtRequestRepository,
            ),
            Provider<DebtRepository>(
              create: (context) => mockDebtRepository,
            ),
            Provider<KeyPairRepository>(
              create: (context) => mockKeyPairRepository,
            )
          ],
          child: SettingsPage(
            onRequestRemoved: () {},
          ),
        ),
      ),
    );

    await tester.pumpAndSettle();

    expect(find.text('Verwijder persoonlijke gegevens'), findsOneWidget);

    await tester.tap(find.byKey(RemovePersonalDataButtonKey));
    await tester.pumpAndSettle();

    expect(
        find.text(
            'Wil je al jouw gegevens verwijderen en de app opnieuw opstarten?'),
        findsOneWidget);

    await tester.tap(find.text('Ja'));
    await tester.pumpAndSettle();

    verify(mockDebtRepository.deleteAllDebt()).called(1);
    verify(mockKeyPairRepository.DeleteKeyPair()).called(1);
    verify(mockDebtRequestRepository.deleteRequest()).called(1);
  });
}
