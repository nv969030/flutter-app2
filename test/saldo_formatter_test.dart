// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/saldo_formatter.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('format saldo', () {
    var eurocents = 100000;
    expect(formatSaldo(eurocents), '€ 1.000,00');
  });
}
