// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht_detail/bloc/schulden_overzicht_detail_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/widgets/schulden_detail_list.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SchuldenOverzichtDetailPage extends StatelessWidget {
  final SchuldenOverzicht schuldenOverzicht;

  SchuldenOverzichtDetailPage({this.schuldenOverzicht});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Schuldenoverzicht',
        ),
      ),
      body: schuldenOverzicht != null
          ? SchuldenDetailList(schuldenOverzicht)
          : BlocBuilder<SchuldenOverzichtDetailBloc,
              SchuldenOverzichtDetailState>(
              builder: (context, state) {
                return state.isLoading || state.schuldenOverzicht == null
                    ? Center(
                        child: CircularProgressIndicator(),
                      )
                    : SchuldenDetailList(state.schuldenOverzicht);
              },
            ),
    );
  }
}
