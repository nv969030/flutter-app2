// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:blauwe_knop/saldo_formatter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'claim_hint_icon.dart';

class SchuldListItem extends StatelessWidget {
  final SchuldDetail _schuld;
  final DateFormat _dateFormat = DateFormat('dd-MM-yyyy');

  SchuldListItem(this._schuld);

  Widget claimText(BuildContext context) {
    return Text(
        '${_schuld.vorderingOvergedragen ? 'Vordering overgedragen' : 'Niet overgedragen'}',
        style: Theme.of(context).textTheme.bodyText1);
  }

  Widget balanceType(BuildContext context) {
    return Text('${_schuld.type}',
        style: Theme.of(context).textTheme.bodyText2);
  }

  Widget balanceText(BuildContext context) {
    return Text('${formatSaldo(_schuld.saldo)}',
        style: Theme.of(context).textTheme.bodyText2);
  }

  Widget claimDebtBalanceDate(BuildContext context) {
    return Text('op ${_dateFormat.format(DateTime.parse(_schuld.saldoDatum))}',
        style: Theme.of(context).textTheme.bodyText1);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child: balanceType(context)),
              balanceText(context),
            ],
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    claimText(context),
                    ClaimHintIcon(),
                  ],
                ),
              ),
              claimDebtBalanceDate(context),
            ],
          ),
        ],
      ),
    );
  }
}
