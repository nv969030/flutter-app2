// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';
import 'dart:developer';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SchuldenOverzichtDetailBloc
    extends Bloc<SchuldenOverzichtDetailEvent, SchuldenOverzichtDetailState> {
  final AppDebtProcessRepository appDebtRepository;
  final AuthorizationRepository authorizationRepository;
  final Organization organization;

  SchuldenOverzichtDetailBloc(
    this.organization,
    this.appDebtRepository,
    this.authorizationRepository,
  ) : super(SchuldenOverzichtDetailState(
          hasError: false,
          isLoading: false,
        )) {
    on<GetSchuldenOverzicht>(_onGetSchuldenOverzicht);

    if (authorizationRepository.isAuthorized(organization.name)) {
      add(GetSchuldenOverzicht());
    }
  }

  Future<void> _onGetSchuldenOverzicht(GetSchuldenOverzicht event,
      Emitter<SchuldenOverzichtDetailState> emit) async {
    emit(state.copyWith(
      isLoading: true,
      hasError: false,
    ));

    try {
      var token = authorizationRepository.getToken(organization.name);
      var schuldenOverzicht = await appDebtRepository.client
          .getSchuldenOverzicht(organization.apiUrl, token);

      emit(state.copyWith(
        isLoading: false,
        schuldenOverzicht: schuldenOverzicht,
      ));
    } catch (exeception) {
      log('failed to get schuldenoverzicht from API: ${exeception.toString()}');
      emit(state.copyWith(
        isLoading: false,
        hasError: true,
      ));
    }
  }
}

class SchuldenOverzichtDetailState {
  bool isLoading;
  bool hasError;
  SchuldenOverzicht schuldenOverzicht;

  SchuldenOverzichtDetailState({
    this.isLoading,
    this.hasError,
    this.schuldenOverzicht,
  });

  SchuldenOverzichtDetailState copyWith({
    bool isLoading,
    bool hasError,
    SchuldenOverzicht schuldenOverzicht,
  }) {
    return SchuldenOverzichtDetailState(
        isLoading: isLoading ?? this.isLoading,
        hasError: hasError ?? this.hasError,
        schuldenOverzicht: schuldenOverzicht ?? this.schuldenOverzicht);
  }
}

class SchuldenOverzichtDetailEvent {
  const SchuldenOverzichtDetailEvent();
}

class GetSchuldenOverzicht extends SchuldenOverzichtDetailEvent {}
