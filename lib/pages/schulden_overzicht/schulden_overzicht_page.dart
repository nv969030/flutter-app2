// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/pages/authentication-wizard/authentication_wizard_page.dart';
import 'package:blauwe_knop/pages/faq/bloc/faqs_bloc.dart';
import 'package:blauwe_knop/pages/faq/faqs_page.dart';
import 'package:blauwe_knop/pages/login/bloc/authentication_bloc.dart';
import 'package:blauwe_knop/pages/login/login_page.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/bloc/schulden_overzicht_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/manual_login_flow.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/registrator_flow.dart';
import 'package:blauwe_knop/pages/schulden_overzicht/widgets/select_flow.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/bloc/schulden_overzicht_detail_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/schulden_overzicht_detail_page.dart';
import 'package:blauwe_knop/pages/settings/settings_page.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/pages/faq/widgets/faq_widget.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class SchuldenOverzichtPage extends StatefulWidget {
  @override
  SchuldenOverzichtPageState createState() {
    return SchuldenOverzichtPageState();
  }
}

class SchuldenOverzichtPageState extends State<SchuldenOverzichtPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    void organizationListRefreshedHandler(BuildContext context) async {
      var result = Completer();

      var schemaBloc = BlocProvider.of<SchuldenOverzichtBloc>(context);
      var stateListener = schemaBloc.stream.listen((state) {
        if (state.organizations.isEmpty) {
          return;
        }

        if (result.isCompleted) {
          return;
        }

        result.complete();
      });

      schemaBloc.add(OrganizationsReload());

      await result.future.then((value) => {stateListener.cancel()});
      return result.future;
    }

    var appDebtRepository = Provider.of<AppDebtProcessRepository>(context);
    var authorizationRepository = Provider.of<AuthorizationRepository>(context);
    var schuldenRequestRepository =
        Provider.of<SchuldenRequestRepository>(context);
    var schemeRepository = Provider.of<SchemeRepository>(context);
    var faqRepository = Provider.of<FaqRepository>(context);
    var appLinkRepository = Provider.of<AppLinkRepository>(context);
    var debtRepository = Provider.of<DebtRepository>(context);
    var sessionProcessRepository =
        Provider.of<SessionProcessRepository>(context);
    var keyPairRepository = Provider.of<KeyPairRepository>(context);

    return BlocProvider<SchuldenOverzichtBloc>(create: (_) {
      var result = SchuldenOverzichtBloc(
          schemeRepository,
          authorizationRepository,
          appLinkRepository,
          debtRepository,
          appDebtRepository,
          schuldenRequestRepository,
          sessionProcessRepository,
          keyPairRepository);
      result.add(OrganizationsReload());
      return result;
    }, child: BlocBuilder<SchuldenOverzichtBloc, SchuldenOverzichtState>(
        builder: (context, state) {
      final schuldenOverzichtBloc =
          BlocProvider.of<SchuldenOverzichtBloc>(context);

      var leadingIconButton;
      if (state.loginPerOrganization) {
        leadingIconButton = IconButton(
          icon: Icon(
            Icons.chevron_left,
            size: 38,
          ),
          onPressed: () {
            schuldenOverzichtBloc.add(DisableLoginPerOrganization());
            schuldenOverzichtBloc.add(OrganizationsReload());
          },
        );
      } else if (state.organizations.isNotEmpty) {
        leadingIconButton = IconButton(
          key: SettingsPageLinkKey,
          icon: Icon(
            Icons.settings,
            size: 24,
          ),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return SettingsPage(
                    onRequestRemoved: () {
                      schuldenOverzichtBloc.add(OrganizationsReload());
                    },
                  );
                },
              ),
            );
          },
        );
      }

      return Scaffold(
        appBar: AppBar(
          title: Text(
            'Blauwe Knop',
          ),
          leading: leadingIconButton,
          actions: <Widget>[
            if (state.organizations.isNotEmpty)
              IconButton(
                icon: Icon(
                  FontAwesomeIcons.questionCircle,
                  size: 24,
                ),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => FaqsPage()),
                  );
                },
              )
          ],
        ),
        body: BlocBuilder<SchuldenOverzichtBloc, SchuldenOverzichtState>(
            builder: (context, state) {
          final showSelectFlow = !state.hasSendDebtRequest &&
              !state.hasUnfinishedDebtRequest &&
              !state.loginPerOrganization;
          final showLoading = showSelectFlow && state.loading;

          if (showLoading) {
            if (state.loading) {
              return Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[CircularProgressIndicator()]),
              );
            }
          }

          if (showSelectFlow) {
            return ListView(
              shrinkWrap: true,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SelectFlow(
                    onStartWizardPressed: () => _navigateToWizard(),
                    onLoginPerOrganizationPressed: () {
                      var bloc =
                          BlocProvider.of<SchuldenOverzichtBloc>(context);
                      bloc.add(LoginPerOrganization());
                    },
                  ),
                ),
                Container(
                  color: Theme.of(context).colorScheme.background,
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: 16.0),
                        Text(
                          'Veelgestelde vragen',
                          style: Theme.of(context).textTheme.headline1,
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(height: 16.0),
                        BlocProvider<FaqsBloc>(
                          create: (context) {
                            return FaqsBloc(
                              faqRepository,
                            );
                          },
                          child: BlocBuilder<FaqsBloc, FaqsState>(
                            builder: (context, state) {
                              final bloc = BlocProvider.of<FaqsBloc>(context);
                              return FaqWidget(
                                faqItems: state.faqItems,
                                onExpandTap: (int index, bool isExpanded) {
                                  bloc.add(FaqItemExpand(index, isExpanded));
                                },
                                backgroundColor:
                                    Theme.of(context).colorScheme.background,
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            );
          }

          if (state.loginPerOrganization) {
            return ManualLoginFlow(
                onOrganizationTap: (org) {
                  if (!org.manualLogin || org.loggedIn) {
                    _navigateToSchuldenOverzicht(
                      appDebtRepository,
                      authorizationRepository,
                      org,
                    );
                  } else {
                    _navigateToLoginPage(
                      appDebtRepository,
                      authorizationRepository,
                      org,
                    );
                  }
                },
                onRefreshHandle: (context) =>
                    organizationListRefreshedHandler(context));
          }

          return RegistratorFlow(
              onOrganizationTap: (org) {
                if (org.schulden != null) {
                  _navigateToSchuldenOverzicht(
                    appDebtRepository,
                    authorizationRepository,
                    org,
                  );
                }
              },
              onStartWizardTap: () => _navigateToWizard(),
              onDismissHasSendDebtRequestConfirmation: () {
                BlocProvider.of<SchuldenOverzichtBloc>(context)
                    .add(DismissedSendDebtRequest());
              },
              onRefreshHandle: (context) =>
                  organizationListRefreshedHandler(context));
        }),
      );
    }));
  }

  void _navigateToSchuldenOverzicht(
    AppDebtProcessRepository appDebtProcessRepository,
    AuthorizationRepository authorizationRepository,
    DebtInformation org,
  ) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return BlocProvider<SchuldenOverzichtDetailBloc>(
          create: (context) {
            return SchuldenOverzichtDetailBloc(
              org.organization,
              appDebtProcessRepository,
              authorizationRepository,
            );
          },
          child: SchuldenOverzichtDetailPage(
            schuldenOverzicht: org.schulden,
          ),
        );
      }),
    );
  }

  void _navigateToLoginPage(
    AppDebtProcessRepository appDebtProcessRepository,
    AuthorizationRepository authorizationRepository,
    DebtInformation org,
  ) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (context) {
        return BlocProvider<AuthenticationBloc>(
          create: (context) {
            return AuthenticationBloc(
              org.organization,
              appDebtProcessRepository,
              authorizationRepository,
            );
          },
          child: LoginPage(
            organization: org.organization,
          ),
        );
      }),
    );
  }

  void _navigateToWizard() {
    var theContext = context;
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => AuthenticationWizardPage(
          onWizardCompletedHandler: () {
            var schuldenOverzichtPage = MaterialPageRoute(builder: (context) {
              return SchuldenOverzichtPage();
            });

            Navigator.popUntil(theContext, (route) {
              return route.isFirst;
            });

            Navigator.of(theContext).pushReplacement(schuldenOverzichtPage);
          },
        ),
      ),
    );
  }
}
