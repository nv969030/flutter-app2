// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/repositories/session_process/responses/complete_challenge_response.dart';
import 'package:blauwe_knop/repositories/session_process/responses/start_challenge_response.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SchuldenOverzichtBloc
    extends Bloc<SchuldenOverzichtEvent, SchuldenOverzichtState> {
  final SchemeRepository _schemeRepository;
  final AuthorizationRepository _authorizationRepository;
  final AppDebtProcessRepository _appDebtProcessRepository;
  final DebtRepository _debtRepository;
  final AppLinkRepository _appLinkRepository;
  final SchuldenRequestRepository _schuldenRequestRepository;
  final SessionProcessRepository _sessionProcessRepository;
  final KeyPairRepository _keyPairRepository;
  StreamSubscription _authorizationUpdatesStreamSubscription;

  SchuldenOverzichtBloc(
      this._schemeRepository,
      this._authorizationRepository,
      this._appLinkRepository,
      this._debtRepository,
      this._appDebtProcessRepository,
      this._schuldenRequestRepository,
      this._sessionProcessRepository,
      this._keyPairRepository)
      : super(SchuldenOverzichtState(
          loading: false,
          loadingFailed: false,
          hasSendDebtRequest: false,
          hasUnfinishedDebtRequest: false,
          loginPerOrganization: false,
          organizations: [],
          showToast: false,
        )) {
    on<OrganizationsReload>(_onOrganizationsReload);
    on<LoginPerOrganization>(_onLoginPerOrganization);
    on<DisableLoginPerOrganization>(_onDisableLoginPerOrganization);
    on<DismissedSendDebtRequest>(_onDismissedSendDebtRequest);

    _authorizationRepository.streamAuthorizationUpdates.listen((event) {
      add(OrganizationsReload());
    });
  }

  @override
  Future<void> close() {
    _authorizationUpdatesStreamSubscription?.cancel();
    return super.close();
  }

  Future<String> _getSessionToken(Organization organization) async {
    debugPrint('[_getSessionToken] start');
    var publicKeyPEM = await _keyPairRepository.GetPublicKeyPEM();
    StartChallengeResponse challenge;

    try {
      challenge = await _sessionProcessRepository.startChallenge(
          organization.sessionProcessUrl, publicKeyPEM);
    } catch (e) {
      debugPrint('[startChallenge] error: $e');
      return null;
    }

    var message = '${challenge.random}${challenge.date}';
    debugPrint('message to sign: $message');

    String signedMessage;
    try {
      signedMessage = await _keyPairRepository.Sign(message);
    } catch (e) {
      debugPrint('[signedMessage] error: $e');
      return null;
    }
    debugPrint('signed message: $signedMessage');

    CompleteChallengeResponse sessionToken;
    try {
      sessionToken = await _sessionProcessRepository.completeChallenge(
          organization.sessionProcessUrl, publicKeyPEM, signedMessage);
      debugPrint('recieved: ${sessionToken.sessionToken}');
    } catch (e) {
      debugPrint('[completeChallenge] error: $e');
      return null;
    }

    debugPrint('[_getSessionToken] end');
    return sessionToken.sessionToken;
  }

  Future<void> _reloadOrganizations(
      Emitter<SchuldenOverzichtState> emit) async {
    emit(state.copyWith(loading: true));
    var now = DateTime.now();

    List<Organization> organizationsResponse;
    try {
      organizationsResponse = await _schemeRepository.client.getOrganizations();
    } catch (e) {
      debugPrint('[getOrganizations] error: $e');
      emit(state.copyWith(
        loading: false,
        loadingFailed: true,
      ));
      return;
    }

    var schuldenRequest = await _schuldenRequestRepository.getRequest();

    if (schuldenRequest == null) {
      var organizations = await _convertDebtRequestsToDebtInformation(
          organizationsResponse, null);
      var organizationsWithoutDebt =
          organizations.where((element) => element.schulden == null);

      if (organizationsWithoutDebt.length == organizations.length) {
        // TODO: go to splashscreen.
        emit(state.copyWith(
          loading: false,
          loadingFailed: true,
          organizations: [],
          hasUnfinishedDebtRequest: false,
          hasSendDebtRequest: false,
          loginPerOrganization: false,
          registratorName: null,
        ));
        return;
      }

      emit(state.copyWith(
        loading: false,
        organizations: organizations,
        hasUnfinishedDebtRequest: true,
      ));

      return;
    }

    var organizations = await _convertDebtRequestsToDebtInformation(
        organizationsResponse, schuldenRequest);

    emit(state.copyWith(
      organizations: organizations,
      loading: false,
      hasSendDebtRequest: schuldenRequest != null && schuldenRequest.id != null,
      showToast: true,
      hasUnfinishedDebtRequest:
          schuldenRequest != null && schuldenRequest.id == null,
      registratorName: schuldenRequest != null &&
              schuldenRequest.registratorOrganization != null
          ? schuldenRequest.registratorOrganization.name
          : '',
      loadingTriggeredAt: now,
    ));

    for (var sourceOrganization in schuldenRequest.sourceOrganizations.values) {
      debugPrint('for ${sourceOrganization.name} start');
      debugPrint('variant 2');

      var org = organizationsResponse
          .firstWhere((element) => element.oin == sourceOrganization.oin);

      SchuldenOverzicht schuldenOverzicht;
      DebtRequestStatus requestStatus;

      if (sourceOrganization.linkToken != null) {
        ///TODO: validate linktoken before using? Is there a method / service for this?
        emit(_setOrganizationStatus(
            organizations, org.oin, SourceOrganizationLinkRequested()));

        var schuldenOverzichtResponse = await _getAndStoreSchuldenOverzicht(
          org.apiUrl,
          org.oin,
          sourceOrganization.linkToken,
        );
        schuldenOverzicht = schuldenOverzichtResponse.schuldenOverzicht;
        requestStatus = schuldenOverzichtResponse.requestStatus;
      } else {
        try {
          var sessionToken = await _getSessionToken(org);

          //TODO: validate session token before using? Is there a method / service for this?

          emit(_setOrganizationStatus(
              organizations, org.oin, SourceOrganizationLinkRequested()));

          sourceOrganization.linkToken = await _appLinkRepository.linkRequest(
            org.appLinkProcessUrl,
            schuldenRequest.id,
            schuldenRequest.registratorOrganization.oin,
            sessionToken,
          );
        } catch (e) {
          debugPrint('error: $e');
          emit(_setOrganizationStatus(
              organizations, org.oin, SourceOrganizationUnreachable()));
          continue;
        }

        ///TODO: validate linktoken before using? Is there a method for this
        // TODO: Check linktoken status

        await _schuldenRequestRepository.saveRequest(schuldenRequest);

        var schuldenOverzichtResponse = await _getAndStoreSchuldenOverzicht(
          org.apiUrl,
          org.oin,
          sourceOrganization.linkToken,
        );
        schuldenOverzicht = schuldenOverzichtResponse.schuldenOverzicht;
        requestStatus = schuldenOverzichtResponse.requestStatus;
      }

      organizations = _updateDebtInformation(
        organizations,
        org,
        false,
        schuldenOverzicht,
        requestStatus,
      );
      emit(state.copyWith(
        organizations: organizations,
      ));

      // VARIANT 1: commented due to depreciation of variant 1
      // if(sourceOrganization == null || schuldenRequest.id == null)
      //   var token = _authorizationRepository.getToken(org.name);
      //   if (token != null) {
      //     requestStatus = SourceOrganizationDebtRequested();
      //     var schuldenOverzichtResponse = await _getAndStoreSchuldenOverzicht(
      //       org.apiUrl,
      //       org.oin,
      //       token,
      //     );
      //     schuldenOverzicht = schuldenOverzichtResponse.schuldenOverzicht;
      //     requestStatus = schuldenOverzichtResponse.requestStatus;
      //   }
      //   organizationWrapper = DebtInformation(
      //     organization: org,
      //     manualLogin: true,
      //     loggedIn: token != null,
      //     schulden: schuldenOverzicht,
      //     status: requestStatus,
      //   );
      // }

      debugPrint('for ${sourceOrganization.name} end');
    }

    emit(state.copyWith(
      hasSendDebtRequest: schuldenRequest != null && schuldenRequest.id != null,
      showToast: true,
      hasUnfinishedDebtRequest:
          schuldenRequest != null && schuldenRequest.id == null,
      registratorName: schuldenRequest != null &&
              schuldenRequest.registratorOrganization != null
          ? schuldenRequest.registratorOrganization.name
          : '',
      loadingTriggeredAt: now,
    ));

    Future.delayed(const Duration(milliseconds: 3000), () {
      add(DismissedSendDebtRequest());
    });
  }

  List<DebtInformation> _updateDebtInformation(
      List<DebtInformation> organizations,
      Organization org,
      bool manualLogin,
      SchuldenOverzicht schuldenOverzicht,
      DebtRequestStatus requestStatus) {
    var debtInformation = DebtInformation(
      organization: org,
      manualLogin: manualLogin,
      schulden: schuldenOverzicht,
      status: requestStatus,
    );
    organizations[organizations.indexWhere(
        (element) => element.organization.oin == org.oin)] = debtInformation;
    return organizations;
  }

  SchuldenOverzichtState _setOrganizationStatus(
      List<DebtInformation> organizations,
      String oin,
      DebtRequestStatus status) {
    organizations
        .firstWhere((element) => element.organization.oin == oin)
        .status = status;

    return state.copyWith(organizations: organizations);
  }

  Future<List<DebtInformation>> _convertDebtRequestsToDebtInformation(
      List<Organization> organizationsResponse,
      SchuldenRequest schuldenRequest) async {
    var organizations = <DebtInformation>[];
    for (var org in organizationsResponse) {
      DebtInformation organizationWrapper;
      if (schuldenRequest != null &&
          schuldenRequest.sourceOrganizations != null &&
          schuldenRequest.sourceOrganizations.containsKey(org.oin)) {
        organizationWrapper = DebtInformation(
          organization: org,
          manualLogin: false,
          schulden: await _debtRepository.getDebt(org.oin),
          status: IncludedInDebtRequest(),
        );
      } else {
        var schulden = await _debtRepository.getDebt(org.oin);
        DebtRequestStatus status;

        if (schulden != null && schulden.schulden.isNotEmpty) {
          status = Expired();
        } else {
          status = NotIncludedInRequest();
        }

        organizationWrapper = DebtInformation(
          organization: org,
          manualLogin: true,
          loggedIn: false,
          schulden: schulden,
          status: status,
        );
      }
      organizations.add(organizationWrapper);
    }
    return organizations;
  }

  Future<void> _onOrganizationsReload(
      OrganizationsReload event, Emitter<SchuldenOverzichtState> emit) async {
    await _reloadOrganizations(emit);
  }

  Future<void> _onLoginPerOrganization(
      LoginPerOrganization event, Emitter<SchuldenOverzichtState> emit) async {
    emit(state.copyWith(loginPerOrganization: true));
  }

  Future<void> _onDisableLoginPerOrganization(DisableLoginPerOrganization event,
      Emitter<SchuldenOverzichtState> emit) async {
    // delete authorization state for organizations manually logged in
    state.organizations.forEach((element) {
      _authorizationRepository.deleteOrganization(element.organization.name);
    });
    emit(state.copyWith(
      loginPerOrganization: false,
    ));
  }

  void _onDismissedSendDebtRequest(
      DismissedSendDebtRequest event, Emitter<SchuldenOverzichtState> emit) {
    if (state.showToast) {
      emit(state.copyWith(showToast: false));
    }
  }

  Future<SchuldenOverzichtResponse> _getAndStoreSchuldenOverzicht(
    String url,
    String oin,
    String token,
  ) async {
    debugPrint('[_getAndStoreSchuldenOverzicht] start');
    var schuldenOverzicht;
    DebtRequestStatus requestStatus;
    try {
      requestStatus = SourceOrganizationDebtRequested();
      schuldenOverzicht = await _appDebtProcessRepository.client
          .getSchuldenOverzicht(url, token);
    } on InvalidTokenException catch (e) {
      debugPrint('InvalidTokenException: $e');
      requestStatus = SourceOrganizationUnreachable();
    } on NoTokenProvidedException catch (e) {
      debugPrint('NoTokenProvidedException: $e');
      requestStatus = SourceOrganizationUnreachable();
    } catch (e) {
      debugPrint('error: $e');
      requestStatus = SourceOrganizationUnreachable();
    }
    if (schuldenOverzicht != null) {
      await _debtRepository.saveDebt(oin, schuldenOverzicht);
      requestStatus = ReceivedDebt();
    }
    debugPrint('[_getAndStoreSchuldenOverzicht] end');
    return SchuldenOverzichtResponse(
        requestStatus: requestStatus, schuldenOverzicht: schuldenOverzicht);
  }
}

class SchuldenOverzichtResponse {
  SchuldenOverzicht schuldenOverzicht;
  DebtRequestStatus requestStatus;

  SchuldenOverzichtResponse({
    this.schuldenOverzicht,
    this.requestStatus,
  });
}

class SchuldenOverzichtState {
  bool loading;
  bool loadingFailed;
  bool hasUnfinishedDebtRequest;
  bool hasSendDebtRequest;
  bool loginPerOrganization;
  String registratorName;
  List<DebtInformation> organizations;
  DateTime loadingTriggeredAt;
  bool showToast;

  SchuldenOverzichtState({
    this.loading,
    this.loadingFailed,
    this.hasUnfinishedDebtRequest,
    this.hasSendDebtRequest,
    this.loginPerOrganization,
    this.registratorName,
    this.organizations,
    this.loadingTriggeredAt,
    this.showToast,
  });

  SchuldenOverzichtState copyWith({
    bool loading,
    bool loadingFailed, //TODO: Currently unused, future improvement show error
    bool hasUnfinishedDebtRequest,
    bool hasSendDebtRequest,
    bool loginPerOrganization,
    String registratorName,
    List<DebtInformation> organizations,
    DateTime loadingTriggeredAt,
    bool showToast,
  }) {
    return SchuldenOverzichtState(
      loading: loading ?? this.loading,
      loadingFailed: loadingFailed ?? this.loadingFailed,
      hasUnfinishedDebtRequest:
          hasUnfinishedDebtRequest ?? this.hasUnfinishedDebtRequest,
      hasSendDebtRequest: hasSendDebtRequest ?? this.hasSendDebtRequest,
      loginPerOrganization: loginPerOrganization ?? this.loginPerOrganization,
      registratorName: registratorName ?? this.registratorName,
      organizations: organizations ?? this.organizations,
      loadingTriggeredAt: loadingTriggeredAt ?? this.loadingTriggeredAt,
      showToast: showToast ?? this.showToast,
    );
  }
}

abstract class DebtRequestStatus {}

class IncludedInDebtRequest extends DebtRequestStatus {}

class SourceOrganizationLinkRequested extends DebtRequestStatus {}

class SourceOrganizationDebtRequested extends DebtRequestStatus {}

class DebtRequestEndStatus extends DebtRequestStatus {}

class SourceOrganizationUnreachable extends DebtRequestEndStatus {}

class Expired extends DebtRequestEndStatus {}

class NotIncludedInRequest extends DebtRequestEndStatus {}

class ReceivedDebt extends DebtRequestEndStatus {}

class DebtInformation {
  Organization organization;
  bool loggedIn;
  bool manualLogin;
  SchuldenOverzicht schulden;
  DebtRequestStatus status;

  DebtInformation({
    this.organization,
    this.manualLogin = true,
    this.loggedIn,
    this.schulden,
    this.status,
  });
}

abstract class SchuldenOverzichtEvent {
  const SchuldenOverzichtEvent();
}

class OrganizationsReload extends SchuldenOverzichtEvent {}

class LoginPerOrganization extends SchuldenOverzichtEvent {}

class DisableLoginPerOrganization extends SchuldenOverzichtEvent {}

class DismissedSendDebtRequest extends SchuldenOverzichtEvent {}
