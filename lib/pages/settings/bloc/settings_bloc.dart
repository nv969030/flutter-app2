// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final SchuldenRequestRepository _schuldenRequestRepository;
  final DebtRepository _debtRepository;
  final KeyPairRepository _keyPairRepository;

  SettingsBloc(
    this._schuldenRequestRepository,
    this._debtRepository,
    this._keyPairRepository,
  ) : super(SettingsState(
          isLoading: true,
        )) {
    on<LoadDebtRequest>(_onLoadDebtRequest);
    on<RemoveDebtRequest>(_onRemoveDebtRequest);
    on<RemoveAllPersonalData>(_onRemoveAllPersonalData);

    add(LoadDebtRequest());
  }

  Future<void> _onLoadDebtRequest(
      LoadDebtRequest event, Emitter<SettingsState> emit) async {
    var debtRequest = await _schuldenRequestRepository.getRequest();

    emit(state.copyWith(
      isLoading: false,
      debtRequest: debtRequest,
    ));
  }

  Future<void> _onRemoveDebtRequest(
      RemoveDebtRequest event, Emitter<SettingsState> emit) async {
    emit(state.copyWith(
      isLoading: true,
    ));

    await _schuldenRequestRepository.deleteRequest();

    emit(state.copyWith(
      isLoading: false,
      debtRequest: null,
    ));
  }

  Future<void> _onRemoveAllPersonalData(
      RemoveAllPersonalData event, Emitter<SettingsState> emit) async {
    emit(state.copyWith(
      isLoading: true,
    ));

    await _schuldenRequestRepository.deleteRequest();
    await _debtRepository.deleteAllDebt();
    await _keyPairRepository.DeleteKeyPair();
    // ignore: unawaited_futures
    _keyPairRepository.GenerateKeyPair();

    emit(state.copyWith(
      isLoading: false,
      debtRequest: null,
    ));
  }
}

class SettingsState {
  bool isLoading;
  SchuldenRequest debtRequest;

  SettingsState({
    this.isLoading,
    this.debtRequest,
  });

  SettingsState copyWith({
    bool isLoading,
    SchuldenRequest debtRequest,
  }) {
    return SettingsState(
      isLoading: isLoading ?? this.isLoading,
      debtRequest: debtRequest,
    );
  }
}

class SettingsEvent {
  const SettingsEvent();
}

class LoadDebtRequest extends SettingsEvent {}

class RemoveDebtRequest extends SettingsEvent {}

class RemoveAllPersonalData extends SettingsEvent {}
