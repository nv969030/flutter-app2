// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/settings/bloc/settings_bloc.dart';
import 'package:blauwe_knop/pages/settings/widgets/info_remove_debt_request.dart';
import 'package:blauwe_knop/pages/settings/widgets/info_remove_personal_data.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  final VoidCallback _onRequestRemoved;

  SettingsPage({@required VoidCallback onRequestRemoved})
      : _onRequestRemoved = onRequestRemoved;

  @override
  SettingsPageState createState() {
    return SettingsPageState();
  }
}

class SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    var schuldenRequestRepository =
        Provider.of<SchuldenRequestRepository>(context);
    var debtRepository = Provider.of<DebtRepository>(context);
    var keyPairRepository = Provider.of<KeyPairRepository>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Instellingen',
        ),
      ),
      body: BlocProvider<SettingsBloc>(
        create: (context) {
          return SettingsBloc(
            schuldenRequestRepository,
            debtRepository,
            keyPairRepository,
          );
        },
        child: BlocBuilder<SettingsBloc, SettingsState>(
          builder: (context, state) {
            final bloc = BlocProvider.of<SettingsBloc>(context);

            return state.isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        state.debtRequest == null
                            ? Text(
                                'Geen openstaand verzoek.',
                                style: Theme.of(context).textTheme.headline1,
                              )
                            : InfoRemoveDebtRequest(
                                debtRequest: state.debtRequest,
                                onRemovalConfirmed: () {
                                  bloc.add(RemoveDebtRequest());
                                  widget._onRequestRemoved();
                                },
                              ),
                        SizedBox(
                          height: 64,
                        ),
                        InfoRemovePersonalData(
                          onRemovePressed: () {
                            showDialog(
                              context: context,
                              builder: (context) {
                                return _buildConfirmationDialog(bloc);
                              },
                            );
                          },
                        ),
                      ],
                    ),
                  );
          },
        ),
      ),
    );
  }

  AlertDialog _buildConfirmationDialog(SettingsBloc bloc) {
    return AlertDialog(
      title: Text('Data verwijderen'),
      content: Text(
        'Wil je al jouw gegevens verwijderen en de app opnieuw opstarten?',
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.of(context).pop(),
          child: Text('Nee'),
        ),
        TextButton(
          onPressed: () {
            bloc.add(RemoveAllPersonalData());
            widget._onRequestRemoved();
            Navigator.of(context)..pop()..pop();
          },
          child: Text('Ja'),
        ),
      ],
    );
  }
}
