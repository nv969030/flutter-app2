// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

var RemoveDebtRequestButtonKey = Key('remove-debt-request-button');

class InfoRemoveDebtRequest extends StatelessWidget {
  final SchuldenRequest _debtRequest;
  final VoidCallback _onRemovalConfirmed;

  InfoRemoveDebtRequest({
    @required SchuldenRequest debtRequest,
    @required VoidCallback onRemovalConfirmed,
  })  : _debtRequest = debtRequest,
        _onRemovalConfirmed = onRemovalConfirmed;

  @override
  Widget build(BuildContext context) {
    var registratorSelected = _debtRequest.registratorOrganization != null;
    var organizationCount = _debtRequest.sourceOrganizations.length;

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Verzoek om schuldgegevens',
            style: Theme.of(context).textTheme.headline1,
          ),
          registratorSelected
              ? Text(
                  'Gemeente ${_debtRequest.registratorOrganization.name} stuurt dit verzoek naar $organizationCount ${organizationCount > 1 || organizationCount == 0 ? 'organisaties' : 'organisatie'}.',
                )
              : Text(
                  'Verzoek voor $organizationCount ${organizationCount > 1 || organizationCount == 0 ? 'organisaties' : 'organisatie'}.',
                ),
          SizedBox(
            height: 16.0,
          ),
          ElevatedButton(
            key: RemoveDebtRequestButtonKey,
            style: ElevatedButton.styleFrom(
              primary: Colors.grey,
            ),
            onPressed: () => _onRemovalConfirmed(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.cancel,
                    color: Colors.grey[900],
                  ),
                  Text(
                    'Verzoek intrekken',
                    style: Theme.of(context).textTheme.button.copyWith(
                          color: Colors.grey[900],
                        ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
