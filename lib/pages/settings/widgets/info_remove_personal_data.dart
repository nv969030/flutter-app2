// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

var RemovePersonalDataButtonKey = Key('remove-personal-data-button');

class InfoRemovePersonalData extends StatelessWidget {
  final VoidCallback _onRemovePressed;

  InfoRemovePersonalData({
    VoidCallback onRemovePressed,
  }) : _onRemovePressed = onRemovePressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Verwijder persoonlijke gegevens',
            style: Theme.of(context).textTheme.headline1,
          ),
          Text(
            'Hiermee wordt het verzoek ingetrokken en worden alle persoonlijke gegevens uit de app verwijderd.',
          ),
          SizedBox(
            height: 16.0,
          ),
          ElevatedButton(
            key: RemovePersonalDataButtonKey,
            style: ElevatedButton.styleFrom(
              primary: Colors.red,
            ),
            onPressed: () => _onRemovePressed(),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Verwijder al mijn gegevens',
                    style: Theme.of(context).textTheme.button.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
