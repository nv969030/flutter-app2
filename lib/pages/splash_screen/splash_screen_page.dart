import 'package:flutter/material.dart';

class SplashScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            Image.asset(
              'assets/splash_screen_icon.png',
              height: 176.0,
              width: 176.0,
            ),
          ]),
          SizedBox(height: 72.0),
          Text('Nog heel even geduld,\n de app wordt ingesteld'),
          SizedBox(height: 48.0),
          Container(
            width: 32,
            height: 32,
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
