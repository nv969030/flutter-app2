// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/login/bloc/authentication_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/bloc/schulden_overzicht_detail_bloc.dart';
import 'package:blauwe_knop/pages/schulden_overzicht_detail/schulden_overzicht_detail_page.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatelessWidget {
  final Organization organization;

  LoginPage({
    @required this.organization,
  });

  @override
  Widget build(BuildContext context) {
    var authenticationRepository =
        Provider.of<AuthorizationRepository>(context);
    var appDebtRepository = Provider.of<AppDebtProcessRepository>(context);
    return Scaffold(
      appBar: _buildAppBar(context),
      body: BlocProvider<AuthenticationBloc>(
        create: (context) {
          return AuthenticationBloc(
            organization,
            appDebtRepository,
            authenticationRepository,
          );
        },
        child: BlocConsumer<AuthenticationBloc, AuthenticationState>(
            listener: (context, state) {
          if (state is LoadingAuthTokenSuccess) {
            launch(
              '${state.organization.loginUrl}?authtoken=${state.authToken}',
              forceSafariVC: false,
            );
          } else if (state is LoadingSessionTokenSuccess) {
            Navigator.of(context).pop();
            Navigator.of(context).push(
              MaterialPageRoute(builder: (context) {
                return BlocProvider<SchuldenOverzichtDetailBloc>(
                  create: (context) {
                    return SchuldenOverzichtDetailBloc(organization,
                        appDebtRepository, authenticationRepository);
                  },
                  child: SchuldenOverzichtDetailPage(),
                );
              }),
            );
          } else if (state is AuthenticationFailure) {
            showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Oeps, er gaat iets fout'),
                    content: Text(
                        'Wij zijn op de hoogte van dit incident. Gelieve het inloggen later opnieuw te proberen.'),
                    actions: <Widget>[
                      ElevatedButton(
                        onPressed: () => Navigator.of(context).pop(),
                        child: Text('Ok'),
                      ),
                    ],
                  );
                });
          }
        }, builder: (context, state) {
          return Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 25),
                child: Text(
                  'Login bij ${organization.name} om uw schulden in te zien.',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
              TextButton(
                style: (TextButton.styleFrom(
                  backgroundColor: Theme.of(context).primaryColor,
                  primary: Colors.white,
                )),
                onPressed: () {
                  var loginBloc = BlocProvider.of<AuthenticationBloc>(context);
                  loginBloc.add(RequestAuthToken());
                },
                child: Text(
                  'Inloggen',
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              )
            ],
          );
        }),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: <Widget>[
          Text('Schuldenoverzicht'),
          Text(
            organization.name,
            style: Theme.of(context)
                .textTheme
                .subtitle2
                .copyWith(color: Colors.white),
          )
        ],
      ),
    );
  }
}
