// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final Organization organization;
  final AppDebtProcessRepository appDebtRepository;
  final AuthorizationRepository authorizationRepository;
  String authToken;
  StreamSubscription _uniLinkStreamSubscription;

  AuthenticationBloc(
    this.organization,
    this.appDebtRepository,
    this.authorizationRepository,
  ) : super(Initial()) {
    on<RequestAuthToken>(_onRequestAuthToken);
    on<ExchangeAuthTokenForSessionToken>(_onExchangeAuthTokenForSessionToken);

    initUniLinks(appDebtRepository.uniLinkStream);
  }

  @override
  Future<void> close() {
    _uniLinkStreamSubscription.cancel();
    return super.close();
  }

  Future<void> _onRequestAuthToken(
      RequestAuthToken event, Emitter<AuthenticationState> emit) async {
    emit(LoadingAuthToken());

    try {
      authToken =
          await appDebtRepository.client.createAuthToken(organization.apiUrl);
      emit(LoadingAuthTokenSuccess(
        organization: organization,
        authToken: authToken,
      ));
    } catch (exception) {
      emit(AuthenticationFailure(error: exception.toString()));
    }
  }

  Future<void> _onExchangeAuthTokenForSessionToken(
      ExchangeAuthTokenForSessionToken event,
      Emitter<AuthenticationState> emit) async {
    emit(LoadingSessionToken());

    try {
      var sessionToken = await appDebtRepository.client
          .exchangeAuthToken(organization.apiUrl, authToken);
      authorizationRepository.addOrganization(
          event.organizationName, sessionToken);

      emit(LoadingSessionTokenSuccess());
    } catch (exception) {
      emit(AuthenticationFailure(error: exception.toString()));
    }
  }

  Future<void> initUniLinks(Stream<String> uniLinkStream) async {
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      _uniLinkStreamSubscription = uniLinkStream.listen((String link) {
        _handleUniLink(link);
      });
    } on PlatformException {
      // Handle exception by warning the user their action did not succeed
      // return?
    }
  }

  void _handleUniLink(String link) {
    if (link == null || !link.contains('blauweknop.app.login')) {
      return;
    }

    var uri = Uri.parse(link);
    var organizationName = uri.queryParameters['organization'];

    add(ExchangeAuthTokenForSessionToken(organizationName: organizationName));
  }
}

abstract class AuthenticationState {
  const AuthenticationState();
}

class Initial extends AuthenticationState {}

class LoadingAuthToken extends AuthenticationState {}

class LoadingAuthTokenSuccess extends AuthenticationState {
  final Organization organization;
  final String authToken;

  const LoadingAuthTokenSuccess({
    @required this.organization,
    @required this.authToken,
  });

  @override
  String toString() =>
      'AuthenticateLoadingAuthTokenSuccess { organization: ${organization.name}, authToken: $authToken }';
}

class LoadingSessionToken extends AuthenticationState {}

class LoadingSessionTokenSuccess extends AuthenticationState {}

class AuthenticationFailure extends AuthenticationState {
  final String error;

  const AuthenticationFailure({@required this.error});

  @override
  String toString() => 'LoginFailure { error: $error }';
}

abstract class AuthenticationEvent {
  const AuthenticationEvent();
}

class RequestAuthToken extends AuthenticationEvent {}

class ExchangeAuthTokenForSessionToken extends AuthenticationEvent {
  final String organizationName;

  const ExchangeAuthTokenForSessionToken({
    @required this.organizationName,
  });

  @override
  String toString() =>
      'ExchangeAuthTokenForSessionToken { organization: $organizationName }';
}
