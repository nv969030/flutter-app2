// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/schulden_request/request.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';
import 'package:blauwe_knop/repositories/universal_link/repository.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

int _getNextStep(int currentStep, bool manualOrganizationSelection) {
  if (currentStep == 1 && !manualOrganizationSelection) {
    return 3;
  }

  return currentStep >= 4 ? currentStep : currentStep + 1;
}

int _getPreviousStep(int currentStep, bool manualOrganizationSelection) {
  if (currentStep == 3 &&
      (manualOrganizationSelection == null || !manualOrganizationSelection)) {
    return 1;
  }
  return currentStep <= 1 ? currentStep : currentStep - 1;
}

class WizardBloc extends Bloc<WizardEvent, WizardState> {
  final SchemeRepository _schemeRepository;
  final SchuldenRequestRepository _schuldenRequestRepository;
  final AppDebtProcessRepository _appDebtRepositoryRepository;
  final KeyPairRepository _keyPairRepository;
  final Function _onWizardCompletedHandler;
  StreamSubscription _uniLinkStreamSubscription;
  final CallBackRepository callbackRepository;

  WizardBloc(
    this._schemeRepository,
    this._schuldenRequestRepository,
    this._onWizardCompletedHandler,
    this.callbackRepository,
    this._appDebtRepositoryRepository,
    this._keyPairRepository,
  ) : super(WizardState()) {
    on<LoadUnfinishedRequest>(_onLoadUnfinishedRequest);
    on<NavigateToNextStep>(_onNavigateToNextStep);
    on<NavigateToPreviousStep>(_onNavigateToPreviousStep);
    on<LoadOrganizations>(_onLoadOrganizations);
    on<UseManualSelection>(_onUseManualSelection);
    on<SelectAllOrganizations>(_onSelectAllOrganizations);
    on<OrganizationsSelected>(_onOrganizationsSelected);
    on<AggregatorSelected>(_onAggregatorSelected);
    on<SubmitRequest>(_onSubmitRequest);
    on<ReceivedExchangeToken>(_onReceivedExchangeToken);

    initUniLinks(callbackRepository.getUniversalLinkStream());
  }

  @override
  Future<void> close() {
    _uniLinkStreamSubscription.cancel();
    return super.close();
  }

  Future<void> _onLoadUnfinishedRequest(
      LoadUnfinishedRequest event, Emitter<WizardState> emit) async {
    var unfinishedRequest = await _schuldenRequestRepository.getRequest();

    if (unfinishedRequest == null) {
      emit(state.copyWith(
        isUnfinishedRequestLoaded: true,
      ));

      return;
    }

    var organizations = await _schemeRepository.client.getOrganizations();
    var organizationsByOIN = {for (var e in organizations) e.oin: e};

    var step = 1;
    if (unfinishedRequest.registratorOrganization != null) {
      step = 4;
    } else if (unfinishedRequest.sourceOrganizations.isNotEmpty) {
      step = 3;
    }

    emit(state.copyWith(
      isUnfinishedRequestLoaded: true,
      manualOrganizationSelection:
          unfinishedRequest.manualOrganizationSelection,
      currentStep: step,
      selectedOrganizations: unfinishedRequest.sourceOrganizations.values
          .map((sourceOrg) => organizationsByOIN[sourceOrg.oin])
          .where((sourceOrg) => sourceOrg != null)
          .toList(),
      selectedRegistrator: unfinishedRequest.registratorOrganization != null
          ? organizationsByOIN[unfinishedRequest.registratorOrganization.oin]
          : null,
      organizations: organizations,
      requestSubmitted: false,
    ));
  }

  void _onNavigateToNextStep(
      NavigateToNextStep event, Emitter<WizardState> emit) {
    emit(state.copyWith(
      currentStep:
          _getNextStep(state.currentStep, state.manualOrganizationSelection),
      comingFromStep: state.currentStep,
    ));
  }

  void _onNavigateToPreviousStep(
      NavigateToPreviousStep event, Emitter<WizardState> emit) {
    emit(state.copyWith(
      currentStep: _getPreviousStep(
          state.currentStep, state.manualOrganizationSelection),
      comingFromStep: state.currentStep,
    ));
  }

  Future<void> _onLoadOrganizations(
      LoadOrganizations event, Emitter<WizardState> emit) async {
    var organizations = await _schemeRepository.client.getOrganizations();
    emit(state.copyWith(
      organizations: organizations,
    ));
  }

  Future<void> _onUseManualSelection(
      UseManualSelection event, Emitter<WizardState> emit) async {
    await _schuldenRequestRepository.saveRequest(SchuldenRequest(
      id: null,
      manualOrganizationSelection: false,
      registratorOrganization: null,
      sourceOrganizations: null,
    ));
    emit(state.copyWith(
      manualOrganizationSelection: true,
      selectedOrganizations: <Organization>[],
      selectedRegistrator: null,
      requestSubmitted: false,
    ));

    add(NavigateToNextStep());
  }

  Future<void> _onSelectAllOrganizations(
      SelectAllOrganizations event, Emitter<WizardState> emit) async {
    var organizations = await _schemeRepository.client.getOrganizations();
    emit(state.copyWith(
        manualOrganizationSelection: false,
        selectedOrganizations: organizations));

    add(NavigateToNextStep());
  }

  Future<void> _onOrganizationsSelected(
      OrganizationsSelected event, Emitter<WizardState> emit) async {
    await _schuldenRequestRepository.saveRequest(
      SchuldenRequest(
          id: null,
          manualOrganizationSelection: state.manualOrganizationSelection,
          sourceOrganizations: {
            for (var org in state.selectedOrganizations)
              org.oin: SourceOrganization(
                oin: org.oin,
                name: org.name,
                apiBaseUrl: org.apiUrl,
              )
          },
          registratorOrganization: null),
    );

    emit(state.copyWith(
      selectedOrganizations: event.organizations,
      selectedRegistrator: null,
      requestSubmitted: false,
    ));

    add(NavigateToNextStep());
  }

  Future<void> _onAggregatorSelected(
      AggregatorSelected event, Emitter<WizardState> emit) async {
    await _schuldenRequestRepository.saveRequest(
      SchuldenRequest(
          id: null,
          manualOrganizationSelection: state.manualOrganizationSelection,
          registratorOrganization: RegistratorOrganization(
            oin: event.aggregator.oin,
            name: event.aggregator.name,
            debtRequestProcessUrl: event.aggregator.registratorUrl,
          ),
          sourceOrganizations: {
            for (var org in state.selectedOrganizations)
              org.oin: SourceOrganization(
                oin: org.oin,
                name: org.name,
                apiBaseUrl: org.apiUrl,
              )
          }),
    );

    emit(state.copyWith(
      selectedRegistrator: event.aggregator,
      requestSubmitted: false,
    ));

    add(NavigateToNextStep());
  }

  Future<void> _onSubmitRequest(
      SubmitRequest event, Emitter<WizardState> emit) async {
    var publicKey = await _keyPairRepository.GetPublicKeyPEM();

    emit(state.copyWith(
      requestSubmitted: true,
      openBrowserForLogin: true,
      organizationOINs:
          state.selectedOrganizations.map((e) => e.oin).toList().join(','),
      publicKeyPEM: publicKey,
    ));
  }

  Future<void> _onReceivedExchangeToken(
      ReceivedExchangeToken event, Emitter<WizardState> emit) async {
    var requestId =
        await _appDebtRepositoryRepository.client.getRequestIdForExchangeToken(
      state.selectedRegistrator.registratorUrl,
      event.exchangeToken,
    );

    if (requestId != null) {
      await _schuldenRequestRepository.saveRequest(SchuldenRequest(
        id: requestId,
        registratorOrganization: RegistratorOrganization(
          oin: state.selectedRegistrator.oin,
          name: state.selectedRegistrator.name,
          debtRequestProcessUrl: state.selectedRegistrator.registratorUrl,
        ),
        manualOrganizationSelection: state.manualOrganizationSelection,
        sourceOrganizations: {
          for (var org in state.selectedOrganizations)
            org.oin: SourceOrganization(
              oin: org.oin,
              name: org.name,
              apiBaseUrl: org.apiUrl,
            )
        },
      ));
      _onWizardCompletedHandler();
    }
  }

  Future<void> initUniLinks(Stream<String> uniLinkStream) async {
    try {
      _uniLinkStreamSubscription = uniLinkStream.listen((String link) {
        _handleUniLink(link);
      });
    } catch (err) {
      debugPrint('error handling unilink $err');
    }
  }

  void _handleUniLink(String link) {
    if (link == null || !link.contains('blauweknop.app.login')) {
      return;
    }
    var uri = Uri.parse(link);

    var requestExchangeToken = uri.queryParameters['requestExchangeToken'];
    add(ReceivedExchangeToken(requestExchangeToken));
  }
}

class WizardState {
  int comingFromStep;
  int currentStep;
  bool isUnfinishedRequestLoaded;
  List<Organization> organizations;
  bool manualOrganizationSelection;
  List<Organization> selectedOrganizations;
  Organization selectedRegistrator;
  bool requestSubmitted;
  bool openBrowserForLogin;
  String organizationOINs;
  String publicKeyPEM;

  WizardState({
    this.comingFromStep = 0,
    this.currentStep = 1,
    this.isUnfinishedRequestLoaded = false,
    this.organizations,
    this.manualOrganizationSelection,
    this.selectedOrganizations,
    this.selectedRegistrator,
    this.requestSubmitted,
    this.openBrowserForLogin,
    this.organizationOINs,
    this.publicKeyPEM,
  });

  WizardState copyWith({
    int comingFromStep,
    int currentStep,
    bool isUnfinishedRequestLoaded,
    List<Organization> organizations,
    bool manualOrganizationSelection,
    List<Organization> selectedOrganizations,
    Organization selectedRegistrator,
    bool requestSubmitted,
    bool openBrowserForLogin,
    String organizationOINs,
    String publicKeyPEM,
  }) {
    return WizardState(
      comingFromStep: comingFromStep ?? this.comingFromStep,
      currentStep: currentStep ?? this.currentStep,
      isUnfinishedRequestLoaded:
          isUnfinishedRequestLoaded ?? this.isUnfinishedRequestLoaded,
      organizations: organizations ?? this.organizations,
      manualOrganizationSelection:
          manualOrganizationSelection ?? this.manualOrganizationSelection,
      selectedOrganizations:
          selectedOrganizations ?? this.selectedOrganizations,
      selectedRegistrator: selectedRegistrator ?? this.selectedRegistrator,
      requestSubmitted: requestSubmitted ?? this.requestSubmitted,
      openBrowserForLogin: openBrowserForLogin ?? this.openBrowserForLogin,
      organizationOINs: organizationOINs ?? this.organizationOINs,
      publicKeyPEM: publicKeyPEM ?? this.publicKeyPEM,
    );
  }
}

abstract class WizardEvent {}

class LoadUnfinishedRequest extends WizardEvent {}

class LoadPersistedState extends WizardEvent {}

class LoadOrganizations extends WizardEvent {}

class UseManualSelection extends WizardEvent {}

class SelectAllOrganizations extends WizardEvent {}

class OrganizationsSelected extends WizardEvent {
  final List<Organization> organizations;

  OrganizationsSelected({@required this.organizations});

  @override
  String toString() => 'OrganizationsSelected { $organizations }';
}

class AggregatorSelected extends WizardEvent {
  final Organization aggregator;

  AggregatorSelected({@required this.aggregator});

  @override
  String toString() => 'AggregatorSelected { $aggregator }';
}

class SubmitRequest extends WizardEvent {}

class ReceivedExchangeToken extends WizardEvent {
  String exchangeToken;
  ReceivedExchangeToken(this.exchangeToken);
}

class NavigateToPreviousStep extends WizardEvent {}

class NavigateToNextStep extends WizardEvent {}
