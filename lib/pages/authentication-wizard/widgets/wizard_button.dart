import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class WizardButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback onPressed;
  final FaIcon buttonIcon;

  const WizardButton(this.buttonText, this.onPressed,
      [this.buttonIcon = const FaIcon(FontAwesomeIcons.externalLinkAlt)]);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () => onPressed(),
      style: ElevatedButton.styleFrom(
        primary: Theme.of(context).primaryColor,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(right: 8.5),
              child: Text(
                buttonText,
                style: Theme.of(context)
                    .textTheme
                    .button
                    .copyWith(color: Colors.white),
              ),
            ),
            buttonIcon ??
                Icon(
                  Icons.chevron_right,
                  color: Colors.white,
                ),
          ],
        ),
      ),
    );
  }
}
