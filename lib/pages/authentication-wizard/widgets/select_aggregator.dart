// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/authentication-wizard/bloc/wizard_bloc.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/back_button.dart'
    as bb;
import 'package:blauwe_knop/widgets/faq_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SelectAggregator extends StatelessWidget {
  static const String routeName = 'select_aggregator';
  final void Function(Organization) submitSelectedAggregator;
  final VoidCallback onBackTapped;

  SelectAggregator({
    @required this.submitSelectedAggregator,
    @required this.onBackTapped,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Stap 2 / 4',
        ),
        leading: bb.CustomBackButton(
          onPressed: () => onBackTapped(),
        ),
        actions: [
          FaqIconButton(),
        ],
      ),
      body: BlocBuilder<WizardBloc, WizardState>(
        builder: (context, state) {
          return SingleChildScrollView(
            child: _buildFilterableAggregatorList(
                context,
                state.organizations
                    .where((element) => element.isRegistrator == true)
                    .toList(),
                submitSelectedAggregator),
          );
        },
      ),
    );
  }
}

Widget _buildFilterableAggregatorList(BuildContext context,
    List<Organization> organizations, Function submitSelectedAggregator) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 35, vertical: 35),
        child: Text(
          'Wie mag uw verzoek om schuldinformatie doorgeven aan deze organisaties?',
          style: Theme.of(context).textTheme.headline1,
          textAlign: TextAlign.center,
        ),
      ),
      ExpandableIntroduction(),
      organizations == null
          ? CircularProgressIndicator()
          : _buildFilterableListView(
              context, organizations, submitSelectedAggregator),
    ],
  );
}

class ExpandableIntroduction extends StatefulWidget {
  @override
  _ExpandableIntroductionState createState() => _ExpandableIntroductionState();
}

class _ExpandableIntroductionState extends State<ExpandableIntroduction> {
  bool expanded = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0, left: 16, right: 16, bottom: 35),
      child: expanded
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'U hoeft niet bij elke organisatie apart in te loggen, maar kiest één organisatie die uw verzoek controleert en beschikbaar stelt aan de andere overheidsorganisaties.\n\n'
                  'Voordat organisaties schuldgegevens doorgeven, willen ze zeker weten dat ze deze informatie aan u geven.\n\n'
                  'Normaal gesproken moet u daarvoor bij iedere organisatie apart inloggen.\n\n'
                  'Bij gebruik van de Blauwe Knop maken we dit makkelijker, waardoor u maar één keer in hoeft te loggen. Daarom kiest u één organisatie die uw verzoek controleert en beschikbaar stelt aan de andere overheidsorganisaties.\n\n'
                  'Uw telefoon zal deze schuldgegevens vervolgens rechtstreeks bij de organisaties ophalen.\n\n'
                  'De organisatie waar u uw verzoek indient krijgt geen informatie over uw schulden te zien.\n\n',
                ),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                  onPressed: () {
                    setState(() {
                      expanded = false;
                    });
                  },
                  style: (TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  )),
                  child: Text(
                    'Verberg informatie',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ],
            )
          : Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'U hoeft niet bij elke organisatie apart in te loggen, maar kiest één organisatie die uw verzoek controleert en doorstuurt naar de andere overheidsorganisaties. \n\nDe organisatie die uw verzoek doorstuurt, krijgt de informatie over uw schulden niet te zien.',
                ),
                SizedBox(
                  height: 10,
                ),
                TextButton(
                  onPressed: () {
                    setState(() {
                      expanded = true;
                    });
                  },
                  style: (TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  )),
                  child: Text(
                    'Hoe werkt dit?',
                    style: TextStyle(
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}

Widget _buildFilterableListView(BuildContext context,
    List<Organization> organizations, Function onAggregatorSelected) {
  return FilterableOrganizationList(
    organizations: organizations,
    onAggregatorSelected: onAggregatorSelected,
  );
}

class FilterableOrganizationList extends StatefulWidget {
  final List<Organization> organizations;
  final Function(Organization) onAggregatorSelected;

  FilterableOrganizationList({
    @required this.organizations,
    @required this.onAggregatorSelected,
  });

  @override
  _FilterableOrganizationListState createState() =>
      _FilterableOrganizationListState();
}

class _FilterableOrganizationListState
    extends State<FilterableOrganizationList> {
  List<Organization> selectedOrganizations = [];
  String query = '';

  @override
  Widget build(BuildContext context) {
    var filteredOrganizations = widget.organizations
        .where((element) =>
            element.name.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 16,
          ),
          child: TextFormField(
            key: Key('filter-aggregator-field'),
            decoration: const InputDecoration(
              prefixIcon: Icon(Icons.search),
              hintText: 'Bijv. uw eigen gemeente',
            ),
            onChanged: (value) {
              setState(() {
                query = value;
              });
            },
          ),
        ),
        filteredOrganizations.isNotEmpty
            ? ListView.separated(
                shrinkWrap: true,
                separatorBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.only(
                      left: 16.0,
                      right: 16.0,
                    ),
                    child: Divider(
                      height: 1,
                      color: Colors.grey[300],
                    ),
                  );
                },
                itemBuilder: (context, index) {
                  var menuOption = filteredOrganizations[index];
                  return ListTile(
                    onTap: () => widget.onAggregatorSelected(menuOption),
                    title: Text(
                      menuOption.name,
                    ),
                    trailing: Icon(
                      Icons.chevron_right,
                      color: Colors.grey,
                    ),
                  );
                },
                itemCount: filteredOrganizations.length,
              )
            : Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text('Geen registrators gevonden'),
              ),
      ],
    );
  }
}
