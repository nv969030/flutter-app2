// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/authentication-wizard/bloc/wizard_bloc.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/back_button.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/wizard_button.dart';
import 'package:blauwe_knop/widgets/faq_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class ConfirmRequest extends StatelessWidget {
  static const String routeName = 'submit_request';
  final void Function() submitRequest;
  final void Function() onBackTapped;

  ConfirmRequest({@required this.submitRequest, @required this.onBackTapped});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Stap 3 / 4',
        ),
        leading: CustomBackButton(
          onPressed: () => onBackTapped(),
        ),
        actions: [
          FaqIconButton(),
        ],
      ),
      body: BlocBuilder<WizardBloc, WizardState>(
        builder: (context, state) {
          if (state.openBrowserForLogin ?? false) {
            launch(
              '${state.selectedRegistrator.registratorUrl}/debt-request-process?organizations=${state.organizationOINs}&publicKey=${Uri.encodeQueryComponent(state.publicKeyPEM)}',
              forceSafariVC: false,
            );
          }
          return Center(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 35),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    'Verzoek indienen bij\n${state.selectedRegistrator.name}',
                    style: Theme.of(context).textTheme.headline1,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Text(
                    'Dien uw verzoek in bij ${state.selectedRegistrator.name}, om uw schulden op te halen bij de verschillende aangesloten organisaties.',
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  WizardButton('Verzoek indienen', submitRequest,
                      FaIcon(FontAwesomeIcons.externalLinkAlt)),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
