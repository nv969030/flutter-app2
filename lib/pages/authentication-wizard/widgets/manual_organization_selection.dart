// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/pages/authentication-wizard/bloc/wizard_bloc.dart';
import 'package:blauwe_knop/pages/authentication-wizard/widgets/back_button.dart';
import 'package:blauwe_knop/widgets/faq_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ManualOrganizationSelection extends StatelessWidget {
  static const String routeName = 'manual_organization_selection';
  final void Function(List<Organization>) submitSelectedOrganizations;
  final void Function() onBackTapped;

  ManualOrganizationSelection({
    @required this.submitSelectedOrganizations,
    @required this.onBackTapped,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Stap 1 / 4',
        ),
        leading: CustomBackButton(
          onPressed: () => onBackTapped(),
        ),
        actions: [
          FaqIconButton(),
        ],
      ),
      body: BlocBuilder<WizardBloc, WizardState>(
        builder: (context, state) {
          return Center(
            child: _buildOrganizationSelectionList(context, state.organizations,
                state.selectedOrganizations, submitSelectedOrganizations),
          );
        },
      ),
    );
  }
}

Widget _buildOrganizationSelectionList(
    BuildContext context,
    List<Organization> organizations,
    List<Organization> selectedOrgs,
    Function submitSelectedOrganizations) {
  var selectedOrganizations = selectedOrgs ?? <Organization>[];

  void onOrganizationsSelected(List<Organization> organizations) {
    selectedOrganizations = organizations;
  }

  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 35, vertical: 35),
        child: Text(
          'Bij welke overheidsorganisaties wilt u uw schulden ophalen?',
          style: Theme.of(context).textTheme.headline1,
          textAlign: TextAlign.center,
        ),
      ),
      organizations == null
          ? CircularProgressIndicator()
          : Expanded(
              child: _buildListView(context, organizations,
                  selectedOrganizations, onOrganizationsSelected),
            ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 16,
                vertical: 16,
              ),
              child: ElevatedButton(
                onPressed: () =>
                    submitSelectedOrganizations(selectedOrganizations),
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor,
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Volgende',
                        style: Theme.of(context)
                            .textTheme
                            .button
                            .copyWith(color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ))
        ],
      )
    ],
  );
}

Widget _buildListView(
    BuildContext context,
    List<Organization> organizations,
    List<Organization> selectedOrganizations,
    Function onOrganizationsSelected) {
  return SelectableOrganizationsList(
    organizations: organizations,
    onOrganizationsSelected: onOrganizationsSelected,
    selectedOrganizations: selectedOrganizations,
  );
}

class SelectableOrganizationsList extends StatefulWidget {
  final List<Organization> organizations;
  final List<Organization> selectedOrganizations;
  final Function(List<Organization>) onOrganizationsSelected;

  SelectableOrganizationsList({
    @required this.organizations,
    @required this.onOrganizationsSelected,
    this.selectedOrganizations,
  });

  @override
  _SelectableOrganizationsListState createState() =>
      _SelectableOrganizationsListState();
}

class _SelectableOrganizationsListState
    extends State<SelectableOrganizationsList> {
  Map<String, Organization> selectedOrganizations;

  @override
  void initState() {
    if (widget.selectedOrganizations != null) {
      selectedOrganizations = {
        for (var e in widget.selectedOrganizations) e.oin: e
      };
    } else {
      selectedOrganizations = {};
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      separatorBuilder: (context, index) {
        return Padding(
          padding: const EdgeInsets.only(
            left: 16.0,
            right: 16.0,
          ),
          child: Divider(
            height: 1,
            color: Colors.grey[300],
          ),
        );
      },
      itemBuilder: (context, index) {
        var menuOption = widget.organizations[index];
        return CheckboxListTile(
          value: selectedOrganizations.containsKey(menuOption.oin),
          title: Text(
            menuOption.name,
          ),
          onChanged: (newValue) {
            setState(() {
              var isSelected =
                  selectedOrganizations.containsKey(menuOption.oin);

              if (!isSelected) {
                selectedOrganizations[menuOption.oin] = menuOption;
              } else {
                selectedOrganizations.remove(menuOption.oin);
              }
            });

            widget
                .onOrganizationsSelected(selectedOrganizations.values.toList());
          },
        );
      },
      itemCount: widget.organizations.length,
    );
  }
}
