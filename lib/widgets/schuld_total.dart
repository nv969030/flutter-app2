// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/saldo_formatter.dart';
import 'package:flutter/material.dart';

class TotalSchuld extends StatelessWidget {
  final String organization;
  final int totalSaldo;

  TotalSchuld({this.organization, this.totalSaldo});

  @override
  Widget build(BuildContext context) {
    var widgets = <Widget>[
      Text(
        'Totale schuld',
        style: Theme.of(context)
            .textTheme
            .headline1
            .copyWith(fontSize: Theme.of(context).textTheme.bodyText2.fontSize),
      ),
      SizedBox(height: 4.0),
    ];
    if (organization != null) {
      widgets.add(Text('Bij $organization',
          style: Theme.of(context).textTheme.bodyText1));
    }
    return Container(
      color: Theme.of(context).colorScheme.background,
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: widgets,
                ),
              ),
              Text(
                formatSaldo(totalSaldo),
                style: Theme.of(context).textTheme.headline1.copyWith(
                    fontSize: Theme.of(context).textTheme.bodyText2.fontSize),
              )
            ]),
      ),
    );
  }
}
