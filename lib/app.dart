// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/pages/schulden_overzicht/schulden_overzicht_page.dart';
import 'package:blauwe_knop/pages/splash_screen/splash_screen_page.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/theme.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BlauweKnopApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var keyPairRepository = Provider.of<KeyPairRepository>(context);
    return StreamBuilder(
      stream: keyPairRepository.GetHasKeyPairStream(),
      builder: (context, stream) {
        return MaterialApp(
            title: 'Blauwe Knop',
            theme: Themes.blauweKnop(),
            home: (stream.hasData && stream.data)
                ? SchuldenOverzichtPage()
                : SplashScreenPage());
      },
    );
  }
}
