// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/app.dart';
import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';
import 'package:blauwe_knop/repositories/app_debt_process/repository.dart';
import 'package:blauwe_knop/repositories/app_link_process/repository.dart';
import 'package:blauwe_knop/repositories/authentication/repository.dart';
import 'package:blauwe_knop/repositories/debt/repository.dart';
import 'package:blauwe_knop/repositories/faq/client.dart';
import 'package:blauwe_knop/repositories/faq/repository.dart';
import 'package:blauwe_knop/repositories/key_pair/repository.dart';
import 'package:blauwe_knop/repositories/scheme/client.dart';
import 'package:blauwe_knop/repositories/scheme/repository.dart';

import 'package:blauwe_knop/repositories/schulden_request/repository.dart';
import 'package:blauwe_knop/repositories/session_process/repository.dart';
import 'package:blauwe_knop/repositories/universal_link/repository.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uni_links/uni_links.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<AppDebtProcessRepository>(
          create: (_) => AppDebtProcessRepository(
            client: SchuldenHTTPClient(),
            uniLinkStream: linkStream,
          ),
        ),
        Provider<DebtRepository>(
          create: (_) => LocalDebtRepository(),
        ),
        Provider<AuthorizationRepository>(
          create: (_) => AuthorizationRepository(),
        ),
        Provider<SchemeRepository>(
          create: (_) => SchemeRepository(
            client: SchemeMemoryClient(
              organizations: [
                Organization(
                  name: 'demo-org',
                  loginUrl: 'http://10.0.2.2:8088/auth',
                  apiUrl: 'http://10.0.2.2:8080/api/v1',
                  registratorUrl: 'http://10.0.2.2:8083',
                  isRegistrator: true,
                  oin: '00000000000000000001',
                  sessionProcessUrl: 'http://10.0.2.2:8087',
                  appLinkProcessUrl:
                      'http://10.0.2.2:8089/auth/request-link-token',
                ),
              ],
            ),
          ),
        ),
        Provider<FaqRepository>(
          create: (_) => FaqRepository(
            client: FaqHTTPClient(
              baseUrl:
                  'https://gitlab.com/commonground/blauwe-knop/mobile-app/-/raw/master/assets/faqs.json',
            ),
          ),
        ),
        Provider<AppLinkRepository>(
          create: (_) => AppLinkHTTPRepository(),
        ),
        Provider<SchuldenRequestRepository>(
          create: (_) {
            var repository = SchuldenRequestSecureStorage();
            repository.deleteRequest();
            return repository;
          },
        ),
        Provider<CallBackRepository>(
          create: (_) => UniLinkRepository(),
        ),
        Provider<KeyPairRepository>(create: (_) {
          var localKeyPairRepository = LocalKeyPairRepository();
          localKeyPairRepository.UpdateKeyPairStream();
          return localKeyPairRepository;
        }),
        Provider<SessionProcessRepository>(
          create: (_) => SessionProcessHTTPRepository(),
        )
      ],
      child: BlauweKnopApp(),
    ),
  );
}
