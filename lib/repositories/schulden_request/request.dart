// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:blauwe_knop/repositories/schulden_request/registrator_organization.dart';
import 'package:blauwe_knop/repositories/schulden_request/source_organization.dart';

class SchuldenRequest {
  String id;
  bool manualOrganizationSelection;
  RegistratorOrganization registratorOrganization;
  Map<String, SourceOrganization> sourceOrganizations;

  SchuldenRequest({
    this.id,
    this.manualOrganizationSelection,
    this.registratorOrganization,
    this.sourceOrganizations,
  });
  SchuldenRequest.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['manualOrganizationSelection'] != null) {
      manualOrganizationSelection = json['manualOrganizationSelection'] as bool;
    }

    if (json['registratorOrganization'] != null) {
      registratorOrganization =
          RegistratorOrganization.fromJson(json['registratorOrganization']);
    }
    if (json['sourceOrganizations'] != null) {
      var organizations = [];
      json['sourceOrganizations'].forEach((v) {
        organizations.add(SourceOrganization.fromJson(v));
      });
      sourceOrganizations = {for (var org in organizations) org.oin: org};
    }
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['id'] = id;
    data['manualOrganizationSelection'] = manualOrganizationSelection;
    if (sourceOrganizations != null) {
      data['sourceOrganizations'] =
          sourceOrganizations.values.map((v) => v.toJson()).toList();
    } else {
      sourceOrganizations = {};
    }

    if (registratorOrganization != null) {
      data['registratorOrganization'] = registratorOrganization.toJson();
    }

    return data;
  }
}
