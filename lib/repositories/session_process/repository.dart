// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:convert';

import 'package:blauwe_knop/repositories/session_process/requests/complete_challenge_request.dart';
import 'package:blauwe_knop/repositories/session_process/requests/start_challenge_request.dart';
import 'package:blauwe_knop/repositories/session_process/responses/complete_challenge_response.dart';
import 'package:blauwe_knop/repositories/session_process/responses/start_challenge_response.dart';
import 'package:http/http.dart' as http;

abstract class SessionProcessRepository {
  Future<StartChallengeResponse> startChallenge(
    String baseUrl,
    String publicKeyPEM,
  );

  Future<CompleteChallengeResponse> completeChallenge(
    String baseUrl,
    String publicKeyPEM,
    String signedMessage,
  );
}

class SessionProcessHTTPRepository extends SessionProcessRepository {
  @override
  Future<StartChallengeResponse> startChallenge(
      String baseUrl, String publicKeyPEM) async {
    var url = Uri.parse('$baseUrl/sessions/startchallenge');
    var response = await http.post(
      url,
      body: json
          .encode(StartChallengeRequest(publicKeyPEM: publicKeyPEM))
          .toString(),
    );
    if (response.statusCode == 200) {
      return StartChallengeResponse.fromJson(json.decode(response.body));
    } else {
      throw '[startChallenge] unexpected status code: ${response.statusCode}';
    }
  }

  @override
  Future<CompleteChallengeResponse> completeChallenge(
      String baseUrl, String publicKeyPEM, String signedChallenge) async {
    var url = Uri.parse('$baseUrl/sessions/completechallenge');
    var response = await http.post(
      url,
      body: json
          .encode(CompleteChallengeRequest(
            publicKeyPEM: publicKeyPEM,
            signedChallenge: signedChallenge,
          ))
          .toString(),
    );
    if (response.statusCode == 200) {
      return CompleteChallengeResponse.fromJson(json.decode(response.body));
    } else {
      throw '[completeChallenge] unexpected status code: ${response.statusCode}';
    }
  }
}
