class CompleteChallengeRequest {
  String publicKeyPEM;
  String signedChallenge;

  CompleteChallengeRequest({this.publicKeyPEM, this.signedChallenge});

  CompleteChallengeRequest.fromJson(Map<String, dynamic> json) {
    publicKeyPEM = json['publicKeyPEM'];
    signedChallenge = json['signedChallenge'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['publicKeyPEM'] = publicKeyPEM;
    data['signedChallenge'] = signedChallenge;
    return data;
  }
}
