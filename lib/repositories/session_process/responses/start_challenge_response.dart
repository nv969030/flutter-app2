// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class StartChallengeResponse {
  String random;
  int date;

  StartChallengeResponse({this.random, this.date});

  StartChallengeResponse.fromJson(Map<String, dynamic> json) {
    random = json['random'];
    date = json['date'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['random'] = random;
    data['date'] = date;
    return data;
  }
}
