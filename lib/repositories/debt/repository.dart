// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:convert';

import 'package:blauwe_knop/repositories/app_debt_process/responses/schuldenoverzicht_response.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

abstract class DebtRepository {
  Future<SchuldenOverzicht> getDebt(String oin);
  Future<void> saveDebt(String oin, SchuldenOverzicht schuld);
  Future<void> deleteAllDebt();
}

class LocalDebtRepository extends DebtRepository {
  final _storage = FlutterSecureStorage();

  static const ALL_DEBT_OINS_KEY = 'all_debt_oins';
  static const DEBT_OIN_PREFIX_KEY = 'debt_';

  @override
  Future<SchuldenOverzicht> getDebt(String oin) async {
    var requestString = await _storage.read(key: '$DEBT_OIN_PREFIX_KEY$oin');
    if (requestString == null) {
      return null;
    }

    return SchuldenOverzicht.fromJson(json.decode(requestString));
  }

  @override
  Future<void> deleteAllDebt() async {
    var oinsAsString = await _storage.read(
      key: ALL_DEBT_OINS_KEY,
    );

    var OINs = splitOINs(oinsAsString);

    OINs.forEach((oin) async {
      await _storage.delete(key: '$DEBT_OIN_PREFIX_KEY$oin');
    });
  }

  @override
  Future<void> saveDebt(String oin, SchuldenOverzicht schuld) async {
    var allDebtOINs = await _storage.read(
      key: ALL_DEBT_OINS_KEY,
    );
    await _storage.write(
      key: ALL_DEBT_OINS_KEY,
      value: appendOIN(oin, allDebtOINs),
    );

    await _storage.write(
        key: '$DEBT_OIN_PREFIX_KEY$oin', value: json.encode(schuld.toJson()));
  }
}

String appendOIN(String oin, String allOINs) {
  if (allOINs == null || allOINs.isEmpty) {
    return '$oin';
  } else {
    return '$allOINs,$oin';
  }
}

List<String> splitOINs(String oinsAsString) {
  if (oinsAsString == null || oinsAsString.isEmpty) {
    return [];
  }

  return oinsAsString.split(',');
}
