// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:rsa_encrypt/rsa_encrypt.dart';

abstract class KeyPairRepository {
  Future<void> GenerateKeyPair();
  Future<String> GetPublicKeyPEM();
  Future<String> Decrypt(String message);
  Future<String> Sign(String messageToSign);
  Future<void> DeleteKeyPair();
  Stream<bool> GetHasKeyPairStream();
  Future<void> UpdateKeyPairStream();
}

class LocalKeyPairRepository extends KeyPairRepository {
  final _storage = FlutterSecureStorage();
  final _rsaHelper = RsaKeyHelper();
  final _publicKeyStorageKey = 'public_key_storage_key';
  final _privateKeyStorageKey = 'private_key_storage_key';
  final _hasKeyPairStreamController = StreamController<bool>();

  @override
  Future<void> GenerateKeyPair() async {
    var keyPair =
        await _rsaHelper.computeRSAKeyPair(_rsaHelper.getSecureRandom());

    await _storage.write(
      key: _publicKeyStorageKey,
      value: _rsaHelper.encodePublicKeyToPemPKCS1(keyPair.publicKey),
      iOptions: IOSOptions(
        accessibility: IOSAccessibility.unlocked_this_device,
      ),
    );

    await _storage.write(
      key: _privateKeyStorageKey,
      value: _rsaHelper.encodePrivateKeyToPemPKCS1(keyPair.privateKey),
    );
    _hasKeyPairStreamController.add(true);
  }

  @override
  Future<String> GetPublicKeyPEM() async {
    var publicKey = await _storage.read(key: _publicKeyStorageKey);
    return publicKey;
  }

  @override
  Future<String> Decrypt(String message) async {
    var privateKey = await _storage.read(key: _privateKeyStorageKey);
    if (privateKey == null) {
      throw 'private key does not exist';
    }

    return decrypt(message, _rsaHelper.parsePrivateKeyFromPem(message));
  }

  @override
  Future<String> Sign(String message) async {
    var privateKey = await _storage.read(key: _privateKeyStorageKey);
    if (privateKey == null) {
      throw 'private key does not exist';
    }

    return _rsaHelper.sign(
        message, _rsaHelper.parsePrivateKeyFromPem(privateKey));
  }

  @override
  Future<void> DeleteKeyPair() async {
    await _storage.delete(key: _privateKeyStorageKey);
    await _storage.delete(key: _publicKeyStorageKey);
    _hasKeyPairStreamController.add(false);
  }

  @override
  Stream<bool> GetHasKeyPairStream() {
    return _hasKeyPairStreamController.stream;
  }

  @override
  Future<void> UpdateKeyPairStream() async {
    if (await GetPublicKeyPEM() == null) {
      await GenerateKeyPair();
    } else {
      _hasKeyPairStreamController.add(true);
    }
  }
}
