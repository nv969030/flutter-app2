// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
import 'package:blauwe_knop/repositories/app_debt_process/client.dart';

class AppDebtProcessRepository {
  final AppDebtProcessClient client;
  final Stream<String> uniLinkStream;

  AppDebtProcessRepository({
    this.client,
    this.uniLinkStream,
  });
}
