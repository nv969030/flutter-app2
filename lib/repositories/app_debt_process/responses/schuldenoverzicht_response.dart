// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

class SchuldenOverzicht {
  String organisatie;
  List<SchuldDetail> schulden;
  int totaalSaldo;
  String saldoDatumLaatsteSchuld;

  SchuldenOverzicht({
    this.organisatie,
    this.schulden,
    this.totaalSaldo,
    this.saldoDatumLaatsteSchuld,
  });

  SchuldenOverzicht.fromJson(Map<String, dynamic> json) {
    organisatie = json['organisatie'];
    schulden = <SchuldDetail>[];
    if (json['schulden'] != null) {
      json['schulden'].forEach((v) {
        schulden.add(SchuldDetail.fromJson(v));
      });
    }
    totaalSaldo = json['totaalSaldo'];
    saldoDatumLaatsteSchuld = json['saldoDatumLaatsteSchuld'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['organisatie'] = organisatie;
    if (schulden != null) {
      data['schulden'] = schulden.map((v) => v.toJson()).toList();
    }
    data['totaalSaldo'] = totaalSaldo;
    data['saldoDatumLaatsteSchuld'] = saldoDatumLaatsteSchuld;
    return data;
  }
}

class SchuldDetail {
  String type;
  int saldo;
  String saldoDatum;
  bool vorderingOvergedragen;

  SchuldDetail(
      {this.type, this.saldo, this.saldoDatum, this.vorderingOvergedragen});

  SchuldDetail.fromJson(Map<String, dynamic> json) {
    type = json['type'];
    saldo = json['saldo'];
    saldoDatum = json['saldoDatum'];
    vorderingOvergedragen = json['vorderingOvergedragen'];
  }

  Map<String, dynamic> toJson() {
    final data = <String, dynamic>{};
    data['type'] = type;
    data['saldo'] = saldo;
    data['saldoDatum'] = saldoDatum;
    data['vorderingOvergedragen'] = vorderingOvergedragen;
    return data;
  }
}
