import 'package:http/http.dart' as http;

abstract class AppLinkRepository {
  Future<String> linkRequest(
    String baseUrl,
    String requestId,
    String registratorOIN,
    String sessionToken,
  );
}

class AppLinkHTTPRepository extends AppLinkRepository {
  @override
  Future<String> linkRequest(String baseUrl, String requestId,
      String registratorOIN, String sessionToken) async {
    var urlEncodedSessionToken = Uri.encodeQueryComponent(sessionToken);
    var url = Uri.parse(
        '$baseUrl?debtRequestId=$requestId&oin=$registratorOIN&sessionToken=$urlEncodedSessionToken');
    var response = await http.get(url);
    if (response.statusCode == 200) {
      return response.body;
    } else {
      throw '[linkRequest] unexpected status code: ${response.statusCode}';
    }
  }
}
