// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:async';

class AuthorizationRepository {
  final Map<String, String> organizations = {};
  final StreamController<String> _streamController = StreamController();
  Stream<String> _stream;

  AuthorizationRepository() {
    _stream = _streamController.stream.asBroadcastStream();
  }

  Stream<String> get streamAuthorizationUpdates => _stream;

  void addOrganization(String organization, String token) {
    organizations[organization] = token;
    _streamController.add(organization);
  }

  void deleteOrganization(String organization) {
    organizations.remove(organization);
    _streamController.add(organization);
  }

  String getToken(String organization) {
    return organizations[organization];
  }

  bool isAuthorized(String organization) {
    return organizations.containsKey(organization);
  }
}
