// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'dart:convert';

import 'package:blauwe_knop/models/organization.dart';
import 'package:blauwe_knop/repositories/scheme/responses/organization_scheme_item_response.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

abstract class SchemeClient {
  Future<List<Organization>> getOrganizations();
}

class SchemeHTTPClient implements SchemeClient {
  String baseUrl;

  SchemeHTTPClient({@required String baseUrl}) {
    this.baseUrl = baseUrl;
  }

  @override
  Future<List<Organization>> getOrganizations() async {
    var url = Uri.parse(baseUrl);
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var result = <Organization>[];

      var responseJson = json.decode(response.body);
      var list = responseJson as List;
      var itemsList =
          list.map((i) => OrganizationSchemeItem.fromJson(i)).toList();

      itemsList.forEach((schemeItem) => {
            result.add(Organization(
                name: schemeItem.name,
                loginUrl: schemeItem.loginUrl,
                apiUrl: schemeItem.apiBaseUrl,
                oin: schemeItem.oin,
                registratorUrl: schemeItem.registratorUrl,
                isRegistrator: schemeItem.isRegistrator,
                appLinkProcessUrl: schemeItem.appLinkProcessUrl,
                sessionProcessUrl: schemeItem.sessionProcessUrl))
          });

      return result;
    } else {
      throw '[getOrganizations] unexpected status code: ${response.statusCode}';
    }
  }
}

class SchemeMemoryClient implements SchemeClient {
  List<Organization> organizations = [];

  SchemeMemoryClient({List<Organization> organizations}) {
    this.organizations = organizations;
  }

  @override
  Future<List<Organization>> getOrganizations() async {
    return Future.value(organizations);
  }
}
