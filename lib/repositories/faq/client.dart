// Copyright © VNG Realisatie 2021
// Licensed under the EUPL

import 'dart:convert';

import 'package:blauwe_knop/models/faq.dart';
import 'package:blauwe_knop/repositories/faq/responses/faq_item_response.dart';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart' show rootBundle;

abstract class FaqClient {
  Future<List<Faq>> getFaqs();
}

class FaqHTTPClient implements FaqClient {
  String baseUrl;

  FaqHTTPClient({@required String baseUrl}) {
    this.baseUrl = baseUrl;
  }

  @override
  Future<List<Faq>> getFaqs() async {
    var url = Uri.parse(baseUrl);
    var result = <Faq>[];
    var response = await http.get(url);
    var responseJson;
    if (response.statusCode == 200) {
      responseJson = json.decode(response.body);
    } else {
      debugPrint('[getFaqs] unexpected status code: ${response.statusCode}');
      responseJson = await _getFaqsFromAssets();
    }
    var list = responseJson['mainEntity'] as List;
    var itemsList = list.map((i) => FaqItemReponse.fromJson(i)).toList();

    itemsList.forEach((faqItemResponse) => {
          result.add(Faq(
            question: faqItemResponse.name,
            answer: faqItemResponse.acceptedAnswer.text,
          ))
        });

    return result;
  }

  Future<dynamic> _getFaqsFromAssets() async {
    var faqsJsonFile = await rootBundle.loadString('assets/faqs.json');
    return json.decode(faqsJsonFile);
  }
}

class FaqMemoryClient implements FaqClient {
  List<Faq> faqs = [];

  FaqMemoryClient({List<Faq> faqs}) {
    this.faqs = faqs;
  }

  @override
  Future<List<Faq>> getFaqs() async {
    return Future.value(faqs);
  }
}
