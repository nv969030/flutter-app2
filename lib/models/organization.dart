// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

import 'package:flutter/foundation.dart';

class Organization {
  String name;
  String loginUrl;
  String apiUrl;
  String oin;
  String registratorUrl;
  bool isRegistrator;
  String appLinkProcessUrl;
  String sessionProcessUrl;

  Organization({
    @required this.name,
    @required this.loginUrl,
    @required this.apiUrl,
    @required this.oin,
    @required this.registratorUrl,
    @required this.isRegistrator,
    @required this.appLinkProcessUrl,
    @required this.sessionProcessUrl,
  });
}
