#!/usr/bin/env bash

declare -x BUILD_NAME=$(git describe --tags)
declare -x BUILD_NUMBER=$(date +%s)

flutter build apk --release --build-number=${BUILD_NUMBER} --build-name=${BUILD_NAME}
flutter build ios --release --no-codesign --build-number=${BUILD_NUMBER} --build-name=${BUILD_NAME}

cd ios
fastlane ios beta
